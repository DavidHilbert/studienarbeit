\chapter{Modelle}

\section{Kompartimentmodelle}

   Prozesse, die den Transport eines Stoffes beinhalten lassen sich in
    idealisierter Form häufig durch folgendes einfaches Modell beschreiben.
    Zu jedem Zeitpunkt ist die Gesamtmenge eines Stoffes auf $n$ Kompartiments
    $K_1,\dots,K_n$ aufgeteilt. Für jedes Kompartiment kann die zu einem
    Zeitpunkt $t$ beinhaltete Stoffmenge mit $m_i(t)$ angegeben werden.
     Durch irgendeine Art von Mechanismen kann ein Teil der
    Stoffmenge $m_i(t)$ im kleinen Zeitraum $dt$ vom Kompartiment $K_i$ nach $K_j$, $i\neq j$ übergehen.
    Die Menge des Stoffes $k_{ij}m_i(t)dt$ die dabei von $K_i$ nach $K_j$ übergeht ist
    proportional zu der momentanen Stoffmenge $m_i(t)$, dem betrachteten
    Intervall $dt$ und der Proportionalitätskonstanten $k_{ij}\geq 0$.

    Die Stoffmenge, die im Zeitraum von $t$ bis $t+dt$ aus $K_i$ in die anderen
    Kompartiments abfließt ergibt sich aus 

    \[
      \sum^n_{\substack{j=1 \\ j\neq i}}k_{ii}m(t)dt \coloneqq \sum^n_{\substack{j=1 \\ j \neq i}} k_{ij}m_i(t)dt
    \]

    mit der Festlegung 

    \[
      k_{ii} \coloneqq \sum^n_{\substack{j=1 \\ j \neq i}}.
    \]
   
    Der Zufluss nach $K_i$ aus den anderen Kompartiments im gleichen Zeitraum
    kann analog mit

    \[
      \sum^n_{\substack{j=1,j\neq i}}k_{ji}m_j(t)dt
    \]
    angegeben werden. Für die Änderung der Stoffmenge in $K_i$ ergibt sich aus
    der Differenz der Zu-und Abflüsse

    \[
      dm_i(t)=\sum^n_{\substack{j=1 \\ j \neq i}} k_{ji}m_j(t)dt
      - k_{ii}m_i(t)dt.
    \]

    Aus diesem Zusammenhang erhalten wir das System von $n$
    Differentialgleichungen

    \begin{equation}
      \begin{aligned}
        \dot{m}_i=\frac{dm_i(t)}{dt}=\sum^n_{\substack{j=1 \\ j \neq i}} k_{ji}m_j(t)dt
        - k_{ii}m_i(t)dt&,\ i=1,\dots,n. 
      \end{aligned}
    \end{equation}

    In Matrixschreibweise wird dieses System zu
    \[
    \left(
    \begin{matrix}
      \dot{m}_1 \\
      \dot{m}_2 \\
      \vdots \\
      \dot{m}_n \\
    \end{matrix}  
    \right)=
    \left(\begin{matrix}
      -k_{11} & k_{12} &\dots & k_{1n} \\
      k_{21} & -k_{22} & \dots & k_{2n} \\
      \vdots &   \\
      k_{n1} & k_{n2}& \dots & -k_{nn} \\
    \end{matrix}\right)
    \left(
    \begin{matrix}
      m_1(t)\\
      m_2(t)\\
      \vdots \\
      m_n(t)\\
    \end{matrix}
    \right)
    \]

  
   Da innerhalb des Modells die Gesamtmasse erhalten wird, gilt
   $\sum\limits^n_{j=1} k_{ij}=0$ mit $k_{ij}\geq 0$\cite{Heuser95}.

\subsection{Kompartimentmodelle in der Epidemiologie}

	  Die Epidemiologie beschäftigt sich mit dem Studium des
    Gesundheitszustands einer gesamten Population. Untersuchungsgegenstand der
    Epidemiologie ist neben den gesundheitlichen Folgen auch die Verbreitung von
    Krankheiten auf Bevölkerungsebene. Diese im gewissen Sinne über der akuten
    Erkrankung stehende Perspektive unterscheidet die Epidemiologie deutlich von
    der klinischen Medizin, in deren Fokus der oder die einzelne PatientIn
    steht. 
    In der Untersuchung der Verbreitung von Krankheiten sind mathematische Modelle ein
    wichtiges Werkzeug der modernen Epidemiologie. Insbesondere spielen die
    vorher eingeführten Kompartimentmodelle eine bedeutende Rolle. Neben diesen
    sind statistische Modelle ein viel genutztes Werkzeug, werden in dieser
    Arbeit jedoch nicht beleuchtet.
    Bei der Modellierung einer Erkrankung kann die Sichtweise der
    Epidemiologie eingenommen werden und Infektionen als Abfolge
    von Zuständen, die Teile einer Population annehmen können, betrachtet
    werden. Durch Einnahme dieser Perspektive ist der Modellierende in der Lage,
    Epidemien mittels Kompartimentmodellen zu beschreiben. Die Komportiments $K_i$
    werden dabei mit Teilen der Population befüllt, die sich gegenwärtig in
    einem bestimmten Stadium der Infektion befinden.
    Als Werkzeug werden Kompartimentmodelle nicht erst seit Verbreitung von
    Computern verwendet. Bereits 1760 hat Daniel Bernoulli ein Kompartimentmodell genutzt, um
    die Auswirkung der Impfung mit dem Kuhpocken-Erreger auf die Ausbreitung der
    Pocken zu untersuchen \cite{Dietz01}. Neben diesem sehr frühen Einsatz haben
    Kompartimentmodelle ihren Nutzen auch in dem Studium der Ausbreitung von HIV
    oder bei der Ausbreitung der \textit{SARS-CoV2} Pandemie in 2020.
    Um ein Kompartimentmodell auf eine Population anwenden zu können, müssen
    Kompartiments gefunden werden, die den Krankheitsverlauf angemessen
    beschreiben. Mit der Anzahl der verwendeten Kompartiments geht die Feinheit
    der Betrachtung einher. So lassen sich alle Erkrankungen durch die
    einfachste Unterteilung in Infizierbare ($S$) (von engl. susceptible) und
    Infiziert ($I$) beschreiben. Mit dieser Unterteilung lässt sich die
    generelle Ausbreitung gut beschreiben, jedoch gehen in dieser sehr groben
    Betrachtungsweise Details verloren, die neben der reinen Anzahl der
    Infizierten von Bedeutung sein können (z.B. Anzahl hospitalisierte Infizierte).
    Weiter Unterteilungen neben Infizierbaren und Infizierten können nicht
    länger Infizierbare ($R$) (von engl. removed) (z.B. durch Ausheilung,
    Impfung), Verstorbene ($D$) (von engl. dead) oder auch der Erkrankung
    Ausgesetzte, jedoch noch nicht Infizierte ($E$) (von engl. exposed) sein.
    Neben Einführung weiterer Zustände können auch Kompartiments weiter
    unterteilt werden. Ein Beispiel für eine solche feinere Unterteilung ist die
    Unterscheidung Infizierter anhand von Lebensalter.
    Epidemiologische Kompartimentmodelle werden nach den genutzten
    Kompartiments und bis zu einem gewisse Grad auch nach den erlaubten
    Übergängen zwischen den Kompartiments benannt. So werden im $SI$-Modell
    Infizierbare und Infizierte betrachtet, im $SIR$-Modell zusätzlich die nicht
    länger Infizierbaren und im $SIS$-Modell wird ein zusätzlicher Mechanismus
    erlaubt, der eine Reinfektion erlaubt.
    In Anlehnung an obige Herleitung eines allgemeinen Kompartimentmodells
    wird nun der Spezialfall des $SI$-Modells betrachtet. In diesem Modell
    werden zwei Kompartiments $S$ und $I$ betrachtet mit einem Mechanismus, der
    aus von $S$ nach $I$ führt. Bei diesem Mechanismus handelt es sich um die
    Erkrankung. Zu jedem Zeitpunkt gilt $N=S+I$ mit der Gesamtpopulation $N$. Die entsprechende Konstante wird hier und in allen Modellen, die
    eine Erkrankung betrachtet mit $\alpha$ bezeichnet und bildet die Anzahl der
    Kontakte eines Infizierten ab, die zu einer Infektion führen. Es ist leicht
    vorstellbar, dass $\alpha$ neben der Infektiösität auch die Mobilität und Menge der
    Kontakte eines Individuums beeinflusst wird. Damit ist $\alpha$ keine für eine Krankheit
    spezifische Größe, sondern auch abhängig von soziokulturellen
    Einflüssen. Für Populationen mit ähnlicher kultureller Prägung wird der
    Zahlenwert von $\alpha$ von vergleichbarer Größe sein. 
    Da im $SI$-Modell lediglich ein Mechanismus betrachtet wird, der von $S$
    nach $I$ führt, ergibt sich die Änderung des Inhalts beider Kompartiments
    aus dem Abfluss aus $S$, bzw. Zufluss nach $I$. Dieser Fluss hängt ab 
    von der Anzahl der zu einem Zeitpunkt $t$ Infizierbaren $S=S(t)$, der Anzahl
    der aktuell Infizierten $I=I(t)$ und $\alpha$. Konkret kann der Fluss
    angegeben werden durch die Wahrscheinlichkeit, dass ein Infiziertes
    Individuum auf ein infizierbares trifft $\frac{S}{N}$ und der Anzahl der
    Individuen, die durch einen Infizierten infiziert werden $\alpha I$.
    Insgesamt erhalten wir damit für die Änderung von $S$ innerhalb des
    Zeitraums $dt$

    \[
      dS=-\alpha \frac{S}{N} I dt.
    \]
    Für die Änderung von $I$ innerhalb des gleichen Zeitraums erhalten wir

    \[
      dI = \alpha \frac{S}{N} I dt.
    \]

    Aus diesen Zusammenhängen folgt das System von Differentialgleichungen
    
      \begin{align} 
        \dot{S}&=\:-\alpha \frac{SI}{N} \label{Eq:S_SI}\\
        \dot{I}&=\quad  \alpha \frac{SI}{N} \label{Eq:I_SI} \\
        N&= \  S+I \label{Eq:Kont_SI}
      \end{align}

    mit Anfangsbedingungen $I(0)=I_0$ und $S(0)=S_0=N-I_0$.

\begin{figure}
  \centering
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \path 
    (S) edge[->] node[above] {$\alpha$} (I);
\end{tikzpicture}
  \caption{$SI$-Modell}
\end{figure}


\subsection{Analytische Lösung des $SI$-Modells}
    Die Differentialgleichungen des $SI$-Modells sind nicht-linear und sind
    nicht direkt, z.B. durch Trennung der Veränderlichen möglich. Jedoch können
    sie durch Ausnutzen der Kontinuitätsbedingung (\ref{Eq:Kont_SI}) in die Form einer
    Bernoulli-Differentialgleichung  gebracht werden. Bei
    Bernoulli-Differentialgleichung handelt es sich um Differentialgleichungen
    des Typs

    \[
      y^{\prime}(x)=f(x)y(x)+g(x)y^{\alpha}(x) \quad , \alpha \notin \{0,1\}
    \]

   Differentialgleichungen dieser Form können mit der Festlegung  

    \begin{equation}\label{Eq:Bernoulli_Einsetzen}
      z(x)=y^{1-\alpha}
    \end{equation}
    
    als lineare gewöhnliche Differentialgleichung 
    
    \[
      z^{\prime}(x)=\left( y^{1-\alpha} \right)^{\prime}=(1-\alpha)(f(x)z(x)+g(x))
    \]
  
    aufgefasst werden. In dieser Form kann die Differentialgleichung mit
    üblichen Methoden für lineare gewöhnliche Differentialgleichungen gelöst
    werden.

    Einsetzen von (\ref{Eq:Kont_SI}) in  (\ref{Eq:S_SI}) liefert den
    Zusammenhang

    \[
      \dot{S}=-\frac{\alpha}{N} \left( N-I  \right)
      I=-\alpha\left( I-\frac{I^2}{N}  \right)
    \]

    und führt mit $z(t)=I^{-1}$ auf die inhomogene lineare Differentialgleichung

    \begin{equation} \label{Eq:SI_lin}
      z^{\prime}(t)=-\dot{I}=-\alpha \left( I-\frac{I^2}{N}\right)=-\alpha
      \left(z-\frac{1}{N}\right).
    \end{equation}

    Die Inhomogenität von (\ref{Eq:SI_lin}) erfordert zunächst das Lösen der
    homogenen Differentialgleichung

    \[
      z^{\prime}_{h}(t)+\alpha z_{h}=0
    \]

    welches mit dem Ansatz $z_h(t)=e^{\lambda t}$ zur Lösung 

    \begin{equation}\label{Eq:SI_hom_sol}
      z_h(t)=Ce^{-\alpha t}
    \end{equation}

    führt. Die homogene Lösung einer Differentialgleichung ist das Verhalten,
    das eine Differentialgleichung zeigt, wenn keine äußeren Einfüsse auf das
    System wirken. Wird die Differentialgleichung durch äußere Einflüsse
    (Inhomogenitäten) gestört, so ist die Lösung $y(t)$ der inhomogenen
    Differentialgleichung nach dem Superpositionsprinzip von der Form
    $y(t)=y_h(t)+y_p(t)$ mit der homogenen Lösung $y_h(t)$ und der partikulären
    Lösung $y_p(t)$, die das ungestörte System an die Störung anpasst.

    \smallskip

    Die partikuläre Lösung einer Differentialgleichung mit Veränderung der
    Konstanten gefunden werden. Bei diesem Ansatz wird der konstante Faktor der
    homogenen Lösung als veränderliche Funktion betrachtet und nun zur Lösung
    der inhomogenen Differentialgleichung genutzt. Für (\ref{Eq:SI_hom_sol})
    bedeutet dies konkret

    \[
      z_p(t)=C(t)e^{-\alpha t}.
    \]

    Nach einsetzen in (\ref{Eq:SI_lin}) entsteht

    \begin{equation*}
    \begin{aligned}
      C^{\prime}(t)e^{-\alpha t}-\alpha C(t)e^{-\alpha t}&=-\alpha C(t)e^{-\alpha
      t}+\frac{1}{N} \\
      C^{\prime}(t)&=\frac{\alpha}{N}e^{\alpha t}
    \end{aligned}
    \end{equation*}

    und nach Integration erhalten wir

    \[
      C(t)=\frac{1}{N}e^{\alpha t} \quad 
    \]

    mit bewusster Vernachlässigung der bei der Integration auftretenden
    Konstanten. Der Beitrag der vernachlässigten Konstanten wird durch den
    konstanten Faktor $C$ der homogenen Lösung kompensiert.
    Damit ergibt sich die partikuläre Lösung zu

    \[
      z_p(t)=\frac{1}{N}.
    \]

    \smallskip
    
    Nun, da homogenen und partikuläre Lösung der Differentialgleichung bekannt
    sind, erhalten wir die allgemeine Lösung

    \[
      z(t)=Ce^{-\alpha t}+\frac{1}{N}=\frac{1+NCe^{-\alpha t}}{N}
    \]
    
    und nach Umstellen von (\ref{Eq:Bernoulli_Einsetzen}) 

    \[
      I(t)=\frac{N}{1+NCe^{-\alpha t}}.
    \]

    Die auftretende Konstante kann bestimmt werden durch Einsetzen der
    Anfangsbedingung $I(0)=I_0$

    \begin{equation}
      C=\frac{1}{I_0} -\frac{1}{N}=\frac{N-I_0}{NI_0}
    \end{equation}
    
    was zur Lösung 

    \[
      I(t)=\frac{N}{1+\frac{N-I_0}{I_0}e^{-\alpha t}}
    \]

    führt. Diese Lösung führt zusammen mit (\ref{Eq:Kont_SI}) auf

    \[
      S(t)=N-I(t)=N-\frac{N}{1+\frac{N-I_0}{I_0}e^{-\alpha t}}.
    \]


\section{Modellfamilie}


	Durch Wahl unterschiedlichster Kompartiments und Mechanismen, die diese
    verbinden, entstehen Modelle, die zur Modellierung des gleichen Modellobjekts herangezogen
    werden können, aufgrund ihrer variierenden Detailauflösung unterschiedliche Erklärungen bieten. 
    Ein Infektionsgeschehen kann durch ein Modell beschrieben werden, dass nur einen Mechanismus der Ansteckung vorsieht,
    werden aber wichtige Mechanismen übersehen, werden die Modellvorhersagen für längere Zeiträume deutlich von dem realen,
    beobachteten Infektionsgeschehen abweichen. 
	  Ausgehend von dem $SI$-Modell kann durch Hinzufügen weiterer
    Kompartiments und einbeziehen weiterer Mechanismen das ursprüngliche Modell so modifiziert werden, dass
    die hinter der Erkrankung stehenden Mechanismen besser abgebildet werden und die Prognosen das reale Geschehen
    besser beschreiben. Mit Erweiterung der Modelle wird es jedoch auch zunehmend schwerer, 
    die Änderungen in den Kompartiments auf einzelne Mechanismen zurückzuführen und die Parameter des Modells exakt
    zu bestimmen. Wird beispielsweise der Schritt vom $SIR$-Modell zum $SEIR$-Modell durch Einführen eines weitern Kompartiments 
    Mechanismus gemacht, so müssen die vorher verwendeten Parameter entsprechend angepasst werden. Betrachte die Genesungsrate $\gamma$ als konstant.
    Zu einem Zeitpunkt $t$ gelte für das $SIR$-Modell
    \[
      \hat{I}=\alpha SI-\gamma I
    \]
    daraus kann $\alpha$ in Abhängigkeit von $\gamma$ zu

    \[\alpha=\frac{\hat{I}+\gamma I}{SI}\]

    bestimmt werden. Mit der Annahme, dass zum Zeitpunkt $t$ für das $SEIR$-Modell ebenfalls die Änderungsrate $\hat{I}$ gilt folgt für 

    \[
      \beta=\frac{\hat{I}+\gamma I}{E}.
    \]
    
    Zum gleichen Zeitpunkt gilt für die Änderung von $E$

    \[
    \hat{E}=\alpha^*SI-\beta E= \alpha^*SI-(\hat{I}+\gamma I)  
    \]

    woraus die angepasste Ansteckungsrate

    \[
    \alpha^*=\frac{\hat{E}+\hat{I}+\gamma I}{SI}=\alpha +\frac{\hat{E}}{SI}  
    \]

    folgt. Um mit dem $SEIR$-Modell die gleiche Änderungsrate $\dot{I}$  zu erzeugen, muss die Ansteckungsrate größer sein als im $SIR$-Modell. Als Erklärung dafür kann die zeitliche Verzögerung aufgrund der Inkubationszeit herangezogen werden. Da Induviduen im $SEIR$-Modell erst nach einer gewissen Zeit infektiös sind und vorher eine Latenzzeit durchlaufen, müssen für die gleiche Änderung zu einem früheren Zeitpunkt mehr Individuen infizert werden. 



\subsection{Mechanismen}


Grundlegend für die Funktionsweise der betrachteten Modellfamilie sind die
Mechanismen, die den Übergang von einem Kompartiment in ein anderes erlauben.
Die Aufgabe des Modellierenden ist es aus einer Vielzahl möglicher Mechanismen
und damit verbundenen Modellen jene zu wählen, die die Vorgänge bestmöglich
beschreiben. Dabei können sowohl die Anzahl der verwendeten Mechanismen als auch
der beschreibende Parameter variiert werden. Die Auswahl der Mechanismen kann
dabei auf experimentellem Wege getroffen werden, als sich auch auf Erkenntnisse
anderer Fachrichtungen, wie z.B. der Medizin stützen.

Der für die Beschreibung von Infektionsdynamik grundlegendste Mechanismus ist
die \textit{Infektion}. Beim Aufeinandertreffen infizierter und infizierbarer
Individuen ist die Weitergabe der Infektion mit einer gewissen
Wahrscheinlichkeit möglich. Diese Weitergabe der Infektion ist abhängig vom
Übertragungsweg und der Infektiösität des erkrankten Individuums. Für die
Modellierung eines Infektionsgeschehens auf Populationsebene muss neben der
Wahrscheinlichkeit der Übertragung auch die durchschnittliche Anzahl von
Kontakten, bei denen eine Infektion möglich ist, in Betracht gezogen werden.
Diese beiden Aspekte werden in Kompartimentmodellen in dem Parameter $\alpha$
berücksichtigt. Während die Wahrscheinlichkeit der Ansteckung eine spezifische
Größe einer Infektion ist, ist die Anzahl der Risikobegegnungen eine Größe, die
kulturellen Einflussfaktoren unterliegt. Daher kann die Größe des Parameters
$\alpha$ zwischen Populationen variieren.

Ein weiterer wichtiger Mechanismus ist die Beendigung der Infektion. Für viele
bekannte Infektionskrankheiten endet die Phase der Infektion nach einer
gewissen Zeit mit der Genesung oder dem Tod des Infizierten. Der Übergang
zwischen den Kompartiments $I$ und $R$ wird in mit dem Parameter $\gamma$
angegeben. Unter optimistischen Annahme der Genesung als Ausgang der Infektion
heißt dieser Parameter hier \textit{Genesungsrate}. Bei nicht Verfügbarkeit
einer Behandlungsmöglichkeit ist dieser Parameter ebenfalls eine spezifische
Größe der Erkrankung und variiert wenig zwischen Populationen. Deutliche
Unterschiede können auftreten, wenn sich Behandlungsmöglichkeiten zwischen
Populationen sind. Angegeben werden kann der Parameter $\gamma$ als Kehrwert der
infektiösen Zeit $\epsilon$. 
Soll in einem Modell für den Ausgang einer Infektion zwischen Genesung und Tod
unterschieden werden, wird zusätzlich ein Kompartiment $D$ mit Parameter $\mu$
eingeführt. Mit dieser Unterscheidung gilt für die Genesungsrate $\gamma^*$
eines Modells ohne diese Differenzierung $\gamma^*=\gamma+\mu$.

Kommt es zwischen Ansteckung und Ausbruch der Infektion zu einer zeitlichen
Verzögerung, kann dies durch einen Übergang von Kompartiment $S$ in ein
Kompartiment $E$ modelliert werden. Vom diesem Kompartiment findet schließlich
der Übergang mit $\beta$ in den Zustand $I$ statt. Die Verbindung zwischen $S$
und $E$ wird durch für die Infektion $\alpha SI$ genutzten Term statt. Das
Kompartiment $E$ dient dabei der Verzögerung. Individuen, die sich gegenwärtig
in diesem Zustand befinden tagen nicht sofort zur weiteren Verbreitung bei.
Durch diese Verzögerung hängt das aktuelle Infektionsgeschehen von vorherigen
Zeitpunkten ab. Dafür gucken wir uns zunächst den Differenzenquotienten 

\[
  \frac{E_{t+1}-E_t}{h}\alpha S_tI_t-\beta E_t
\]
an und finden durch Umstellen

\[
  E_{t+1}=(1-\beta h)E_t+\alpha h S_tI_t
  \]

  Eine Vorschrift zur Berechnung des jeweils nächsten Zeitschritts. Zusammen mit
  dem Differenzenquotienten

  \[\frac{I_{t+1}-I_t}{h}=\beta E_t-\gamma I_t\]

  für die Änderung des Kompartiment $I$ folgt 

  \[
    I_{t+1}=(1-\gamma h)I_t + \beta h (1-\beta h)E_{t-1}+\alpha \beta
    h^2S_{t-1}I_{t-1}
  \]

  und damit der Bezug auf vergangene Zeitpunkte. 
  Für $\beta \rightarrow \infty$, bzw. $\beta \rightarrow 0$ führt das
  $SEIR$-Modell auf das $SIR$-Modell, bzw. das $SI$-Modell.
 
  Ein weiterer Mechanismus, der für eine Vielzahl von Infektionskrankheiten von
  Bedeutung ist, ist die \textit{Reinfektion}, also die erneute Infektion,
  nach vormaliger Genesung. In Kompartimentmodellen wird dieser Mechanismus
  durch einen Übergang von $R$ nach $S$ mit dem Parameter $\eta$ ermöglicht. 



\begin{table}[H]\label{Tab_Parameter}
\centering
  \begin{tabular}{p{0.05\textwidth} | p{0.9\textwidth}}

    $\alpha$ & Die  sogenannte Transmissionsrate und beschreibt die durchschnittliche
    Anzahl der Kontakte, bei denen es zu einer Infektion kommen kann. Entsprechend
    ist dieser Parameter abhängig von soziokulturellen Faktoren.\\
    \hline
    $\beta$ & Der Kehrwert dieses Parameters ist die Latenzzeit $\sigma$. Bei diesem
    Parameter handelt es sich um eine für eine Infektion charakteristische Größe. \\
    \hline
    $\gamma$ & Hierbei handelt es sich um die Genesungsrate, deren Kehrwert die
    infektiöse Zeit $\epsilon$ ist. Diese Größe ist ebenfalls typisch für eine
    Erkrankung. \\
    \hline
    $\eta$ & Resultiert eine überstandene Infektion nicht in dauerhafter Immunität,
    so beschreibt $\eta$ die Rate, mit der es zu  einer Reinfektion kommt. \\
    \hline
    $\mu$ & Die für die Erkrankung spezifische Mortalitätsrate.\\

  \end{tabular}
\caption{Beschreibung der in den Modellen verwendeten Parameter}
\end{table}

\subsection{Hierarchisierung}

	  Kompartimentmodelle können auf unterschiedliche Weise in eine
    hierarchische Struktur eingeordnet werden. Hier werden die Modelle durch die
    Anzahl der in ihnen verwendeten Parameter strukturiert. So
    ist ein Modell, das durch Einführen eines weiteren Kompartiments aus einem
    Modell hervorgeht, auf der gleichen Hierarchieebene einzuordnen, wie die
    Erweiterung des selben Modells um einen weiteren Mechanismus.
    So sind, z.B. das $SIR$-Modell (\ref{Mo:SIR}) und das $SIS$-Modell (\ref{Mo:SIS})
    auf der selben Hierarchieebene angeordnet, da in beiden Modellen die gleiche Anzahl Parameter genutzt
    wird. 
    Eine Möglichkeit, diese Struktur bei der Suche nach einem Modell, das ein
    Infektionsgeschehen beschreibt, zu nutzen besteht darin, zunächst zu
    versuchen, ein Modell der unteren Hierarchieebenen an die gegebenen Daten
    anzupassen. Ist dabei bereits vor Beginn der Modellierung bekannt, dass
    einem bestimmten Kompartiment eine wichtige Rolle zukommt, kann die Suche
    nach einem passenden Modell auf der niedrigsten Hierarchieebene begonnen
    werden, auf der das entsprechende Kompartiment genutzt wird. Ist ein solches
    niedrighierarchisches Modell gefunden, kann ausgehend von diesem nach einem
    Modell der nächsthöheren Hierarchieebene gesucht werden. Die Idee dabei ist,
    Mechanismen zu Beginn grob zu beschreiben und im Laufe der Modellierung eine
    feinere Unterteilung vorzunehmen. Angenommen die Dynamik einer neuartigen
    Infektionskrankheit kann in guter Näherung durch das $SIR$-Modell
    beschrieben werden. Die Suche nach einem Modell der nächsthöheren
    Hierarchieebene kann beispielsweise durch Einführen eines
    Kompartiment $E$ vorgenommen werden. Ausgehend von den Parametern des
    $SIR$-Modells können die Parameter des $SEIR$-Modells gefunden werden.
    Beschreibt das dabei gefundene $SEIR$-Modell das Infektionsgeschehen besser
    als das $SIR$-Modell, so kann erneut nach einem Modell der nächsten
    Hierarchieebene gesucht werden. Wird die Beschreibung nicht verbessert,
    verbleibt das $SIR$-Modell als beste Näherung oder es kann versucht werden,
    eine Verbesserung durch weitere Mechanismen der nächsten Hierarchieebene zu
    erzielen.
    
    
\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \node (0) at (0,0) {\{$\alpha$\}};
    \node (1) at (-0.8,1) {\{$\alpha$,$\gamma$\}};
    \node (2) at (0.8,1) {\{$\alpha$,$\eta$\}};
    \node (3) at (-1.3,2) {\{$\alpha$,$\gamma$,$\eta$\}};
    \node (4) at (1.3,2) {\{$\alpha$,$\beta$,$\gamma$\}};
    \node (5) at (-1.3,3) {\{$\alpha$,$\beta$,$\gamma$,$\eta$\}};
    \node (6) at (1.3,3) {\{$\alpha$,$\beta$,$\gamma$,$\mu$\}};
    \path
      (0) edge (1)
      (0) edge (2)
      (1) edge (3)
      (1) edge (4)
      (2) edge (4)
      (2) edge (3)
      (3) edge (5)
      (4) edge (5)
      (4) edge (6);
  \end{tikzpicture}
  \caption{Hierarchische Struktur der betrachteten Modelle. Die Ebenen der
  Modellhierarchie ergeben sich aus der Anzahl der notwendigen Parameter eines
  Modells}
  \label{Familie}
\end{figure}


\section{Modelle}
\subsection{SI- Modell}\label{Mo:SI}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \path 
    (S) edge[->] node[above] {$\alpha$} (I);
\end{tikzpicture}
\end{center}
$$
\begin{aligned}
\dot{S} &=& - \alpha SI \\
\dot{I} &=& \alpha SI \\
N&=& S+I \\
\end{aligned}
$$

\subsection{SIS-Modell}\label{Mo:SIS}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \draw[->] (I) -- ++(0,0.7) -- node[above]{$\eta$} ++(-1.5,0) --(S);
  \path 
    (S) edge[->] node[above] {$\alpha$} (I);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S} &=& - \alpha SI +\eta I \\
\dot{I} &=& \alpha SI -\eta I \\
N&=& S+I \\
\end{aligned}
$$

\subsection{SIR-Modell}\label{Mo:SIR}

\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \node[draw, rectangle, minimum size=.5cm] (R) at (3,0) {R};
  \path 
    (S) edge[->] node[above] {$\alpha$} (I)
    (I) edge[->] node[above] {$\gamma$} (R);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S}&=& -\alpha SI \\
\dot{I}&=& \alpha SI - \gamma I \\
\dot{R}&=& \gamma I \\
N&=&S+I+R\\
\end{aligned}
$$


\subsection{SIRS-Modell}\label{Mo:SIRS}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \node[draw, rectangle, minimum size=.5cm] (R) at (3,0) {R};
  \draw[->] (R) -- ++(0,0.7) -- node[above]{$\eta$} ++(-3,0) --(S);
  \path 
    (S) edge[->] node[above] {$\alpha$} (I)
    (I) edge[->] node[above] {$\gamma$} (R);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S}&=& -\alpha SI +\eta R \\
\dot{I}&=& \alpha SI - \gamma I \\
\dot{R}&=& \gamma I -\eta R\\
N&=&S+I+R\\
\end{aligned}
$$

\subsection{SEIR-Modell}\label{Mo:SEIR}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (E) at (1.5,0) {E};
  \node[draw, rectangle,minimum size=.5cm] (I) at (3,0) {I};
  \node[draw, rectangle, minimum size=.5cm] (R) at (4.5,0) {R};
  \path 
    (S) edge[->] node[above] {$\alpha$} (E)
    (E) edge[->] node[above] {$\beta$} (I)
    (I) edge[->] node[above] {$\gamma$} (R);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S}&=& -\alpha SI \\
\dot{E}&=& \alpha SI - \beta E \\
\dot{I}&=& \beta E - \gamma I \\
\dot{R}&=& \gamma I \\
N&=&S+E+I+R\\
\end{aligned}
$$

\subsection{SEIRS-Modell}\label{Mo:SEIRS}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (E) at (1.5,0) {E};
  \node[draw, rectangle,minimum size=.5cm] (I) at (3,0) {I};
  \node[draw, rectangle, minimum size=.5cm] (R) at (4.5,0) {R};
  \draw[->] (R) -- ++(0,0.7) -- node[above]{$\eta$} ++(-4.5,0) --(S);
  \path 
    (S) edge[->] node[above] {$\alpha$} (E)
    (E) edge[->] node[above] {$\beta$} (I)
    (I) edge[->] node[above] {$\gamma$} (R);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S}&=& -\alpha SI +\eta R\\
\dot{E}&=& \alpha SI - \beta E \\
\dot{I}&=& \beta E - \gamma I \\
\dot{R}&=& \gamma I -\eta R\\
N&=&S+E+I+R\\
\end{aligned}
$$


\subsection{SEIRD-Modell}\label{Mo:SEIRD}
\begin{center}
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (E) at (1.5,0) {E};
  \node[draw, rectangle,minimum size=.5cm] (I) at (3,0) {I};
  \node[draw, rectangle, minimum size=.5cm] (R) at (4.5,0) {R};
  \node[draw, rectangle, minimum size=.5cm] (D) at (4.5,-1.2) {D};
  \draw[->] (I) -- ++(0,-1.2) -- node[above]{$\mu$} ++(D);
  \path 
    (S) edge[->] node[above] {$\alpha$} (E)
    (E) edge[->] node[above] {$\beta$} (I)
    (I) edge[->] node[above] {$\gamma$} (R);
\end{tikzpicture}
\end{center}

$$
\begin{aligned}
\dot{S}&=& -\alpha SI\\
\dot{E}&=& \alpha SI - \beta E \\
\dot{I}&=& \beta E - (\gamma + \mu) I \\
\dot{R}&=& \gamma I\\
\dot{D}&=& \mu I \\
N&=& S+E+I+R+D \\
\end{aligned}
$$