\chapter{Künstliche neuronale Netze}\cite{Brauer19}

	  Bei sogenannten künstlichen neuronalen Netzen (KNN) handelt es sich um
    eine Form von Algorithmus, der versucht die Funktionsweise des menschlichen
    Gehirns nachzuahmen. Um dies zu erreichen, werden künstliche Neuronen in
    Schichten angeordnet und untereinander verbunden. Ein künstliches Neuron nutzt die
    Ausgabewerte der mit ihm verbundenen Neuronen einer vorherigen Schicht,
    gewichtet diese, wendet eine im allgemeinen nicht-lineare Funktion auf diese
    Summe an und bildet so seinen eigenen Ausgabewert, der dann erneut als
    Eingangswert für Neuronen der nächsten Schicht dient. Mit dieser Vorschrift
    soll die  Verhalten realer Neuronen nachgeahmt werden. Vereinfacht ausgedrückt
    benötigen diese eine gewisse Aktivierungsenergie, um \textit{aktiviert} zu
    werden und Signal weiterzuleiten. Diese Aktivierungsenergie stammt von Neuronen anderer
    Hirnregionen, die bereits ein Signal weiterleiten. Bildlich gesprochen sind
    Neuronen träge und benötigen eine individuelle Menge an Zuspruch anderer
    Neuronen, um ebenfalls die Entscheidung zur Aktivierung zu treffen.
    Lernen meint in diesem Bild die Anpassung der nötigen Aktivierungsenergie,
    bzw. die Gewichtung und Verbrüderung mit Neuronen bestimmter
    Hirnarealen. Weiter in diesem Bild bleibend könne wir uns gut vorstellen,
    dass die Gehirne begeisterter SportlerInnen gelernt haben, Signale aus den
    Arealen, die für das Sehen zuständig sind, verstärkt mit Hirnregionen, die
    Bewegung verarbeiten, zu verknüpfen.
    Durch Anordnung dieser künstlichen Neuronen in einer Netzstruktur erhofft man
    sich durch Kombination vieler Neuronen ein Verhalten ähnlich zu dem eines Gehirns zu
    erreichen. Besonders an der Funktionsweise realer Gehirne ist die Fähigkeit,
    in gegebenen Daten Muster zu finden und diese mit anderen Daten zu
    verknüpfen. Diese Verknüpfung ist auch dann möglich, wenn die Ein-und
    Ausgangsdaten scheinbar nicht zusammenpassen. So ist es den meisten Menschen
    möglich, unterschiedliche Tierarten oder Instrumenten
    anhand ihrer Klangcharakteristika zu erkennen. Dies gelingt in den meisten
    Fällen auch dann, wenn die verfügbaren Daten nicht vollständig oder gestört
    sind. Mit klassischen algorithmischen Methoden ist diese Unterscheidung für
    Maschinen  nur schwer möglich oder sogar unmöglich, da
    sich auch innerhalb relativ kleiner Gruppen von Tieren wie \textit{Hunde} oder
    \textit{Katzen} sehr verschiedenartige Ausprägungen von Merkmalen möglich
    sind und unklar ist, welche Merkmale konkret zur Zuordnung zu einer
    Gruppe führen. Für die Lösung solche Aufgaben der Mustererkennung können KNN genutzt
    werden, da diese in der Lage sind, selbständig Muster in den Daten zu
    erkennen und entsprechend zu entscheiden. 

	  Das Verhalten eines KNN, eingehenden Daten einen Wert zuzuweisen, kann
    als Funktion der Form $f_{\boldsymbol{\theta}}:\mathbb{R}^n \rightarrow \mathbb{R}^m$ beschrieben werden. 

    Ein typischer Anwendungsfall von neuronalen Netzen sind
    Klassifikationsprobleme. Bei dieser Art von Problemen sollen aus einer
    Menge \newline $\mathcal{S}=\{(\mathbf{x}^{(1)},y^{(1)},\dots
    , (\mathbf{x}^{(M)},y^{(M)}))\},
    \mathcal{X}^d \times \{0,1\}$ von $M$ Objekten $\mathbf{x}^{(i)}$, denen
    ein bekanntes Label $y^{(i)}$ zugeordnet ist,  Zusammenhänge gelernt werden, die
    es ermöglichen, Eingangsdaten mit unbekanntem Label dasjenige mit der
    höchsten Wahrscheinlichkeit zuzuordnen. Gesucht ist eine durch $\theta\in
    \Theta$ parametrisierte Hypothese-Funktion
    $h_{\theta}:\mathcal{X}\rightarrow \{0,1\}$, die jedem $x\in\mathcal{X}$ ein
    Label $y\in\{0,1\}$ zuordnet. 
    
    \begin{definition}{Sigmoidfunktion} Die Funktion 

      \begin{align}
        \sigma:\mathbb{R}\rightarrow (0,1),\ \sigma(x)=\frac{1}{1+e^{-x}}
      \end{align}

      heißt Sigmoidfunktion.

    \end{definition}
    
    Mit der Sigmoidfunktion und einer linearen Transformation 
    $\mathbf{w}^T\mathbf{x}+b$ mit Parametern $\theta=(\mathbf{w},b)$ wird für
    die Wahrscheinlichkeit eines $x$ in einer der beiden Kategorien zu fallen
    der Ansatz

    \begin{align*}
      p(y=1|x;\theta)=\sigma(\mathbf{w}^T\mathbf{x}+b) &\quad &p(y=0|x;\theta)=1-\sigma(\mathbf{w}^T\mathbf{x}+b
    \end{align*}

    gemacht. Für entsprechend gewählte $\theta$ kann mit der Hypothese

    \[
      h_{\theta}=\begin{cases}
        1 &, \sigma(\mathbf{w}^T\mathbf{x}+b)\geq 0.5 \\
        0&,\ \text{sonst}
      \end{cases}=
      \begin{cases}
        1 &, \mathbf{w}^T\mathbf{x}+b \geq 0 \\
        0 &,\ \text{sonst}
      \end{cases}
    \]

    die Zuordnung eines $x$ zu einer der beiden Kategorien vorgenommen werden.
    Der Übergang von der Betrachtung der Wahrscheinlichkeiten zur Auswertung der
    linearen Abbildung fordert lineare Trennbarkeit der Menge. Lineare
    Trennbarkeit ist genau das Kriterium, das in der zweiten Fassung der
    Hypothese zur Unterscheidung der beiden Fälle genutzt wird. 
    Bis zu diesem Punkt handelt es sich um reine logistische Regression. Ist
    eine Menge nicht lineare trennbar, so kann versucht werden eine
    Basisfunktion $\boldsymbol{\phi}:\mathbb{R}^d\rightarrow \mathbb{R}^n$ zu finden, sodass
    eine nach Anwendung dieser Basisfunktion die Zuordnung zu einer Kategorie
    durch Hypothese 

    \[
    h_{\theta}=
    \begin{cases}
      1 &, \mathbf{w}^T\boldsymbol{\phi}(\mathbf{x})+b \geq 0 \\
      0 &,\ \text{sonst}
    \end{cases}
    \]
    
    möglich wird. Für einige Beispiele ist es möglich, diese
    Basisfunktionen zu finden. Für komplexer Beispiele stellt sich die Suche
    nach passenden Basisfunktionen nicht immer als einfach heraus. In diesem
    Punkt unterscheiden sich KNN deutlich von der reinen logistischen
    Regression. Da innerhalb eines KNN parametrisierbare, nicht-lineare
    Funktionen verwendet werden, führt das Training automatisch auf
    Basisfunktionen, die zu linearer Trennbarkeit führen.

	
	  In dieser Arbeit werden keine Klassifikationsprobleme betrachtet, daher
    sollen obige Ausführungen zu diesen Problemen und dem Nutzen KNN nur der
    Vollständigkeit und der Illustration einer Anwendung dienen, bei der KNN
    häufig zum Einsatz kommen. Die in dieser Arbeit zu lösenden Probleme fordern
    nicht die Zuordnung eines Elements zu einer Kategorie, sondern vielmehr die
    stückweise Approximation nicht-linearer Funktionen. 



\section{Historische Bemerkungen}

\begin{comment}
\begin{itemize}
  \item Optional
\end{itemize}
\end{comment}

\section{Grundlagen}

 Neuronale Netze können als Verfahren verstanden werden, das mittels einer
Menge parametrisierbarer- im allgemeinen nichtlinearer-  Funktionen und einer Menge $\mathcal{S}=\{ (\mathbf{x}^{(1)},\mathbf{y}^{(1)}),\dots
 , (\mathbf{x}^{(M)},\mathbf{y}^{(M)})\}$ von Trainingsdaten selbständig eine Funktion
 $f:\mathbb{R}^d \rightarrow \mathbb{R}^{n_L}$ findet, die einen Zusammenhang
 zwischen den Inputvektoren $\mathbf{x}^{(i)}$ und den Zielwerten
 $\mathbf{y}^{(i)}$ herstellt. 
 
 In dieser Arbeit entsprechen die Inputvektoren $\mathbf{x}^{(i)}$ Lösungen
 gerechneter Modelle, deren Parameter $\mathbf{y}^{(i)}$ bekannt sind. Ziel des
 Trainings- also dem Finden der Funktion, die Ein- und Ausgangsdaten verknüpft-
 ist aus bekannten auf unbekannte Daten zu schließen. 


KNN bestehen aus untereinander verbundener Schichten künstlicher Neuronen.
  Die Anzahl der Schichten, die Tiefe, wird mit $L$ angegeben, die Breite der
    einzelnen Schichten mit $n_l, \ l=1,\dots,L$. Eine Schicht eines KNN besteht
    aus $n_l$ künstlicher Neuronen der Form,

\begin{figure}[H]
  \begin{center}
    \begin{tikzpicture}[node distance=1cm, auto]
      \node[draw, circle, minimum size=.7cm] (1) at (0,1) {$x_1$};
      \node[below of=1] (2) {$\vdots$};
      \node[draw, circle, minimum size=.7cm,below of=2] (4) {$x_d$};
      \node[draw, ellipse] (3) at (2,0) {$g\  \mid \ y $};
    \path
      (1) edge[->] node {}(3)
      (3) edge[->] node {}(4);
    \end{tikzpicture}
  \end{center}
\end{figure}

die parallel durchlaufen werden. Verstanden werden kann ein solches Neuron als
    Regression mit Funktion $g$. Werden mehrerer dieser Regressionen parallel
    betrachtet, entsteht die namensgebende Netzstruktur. Sind Tiefe $L$ und
    Breiten der Schichten $n_l$ festgesetzt, entsteht die Funktion
    $f_{\boldsymbol{\theta}}:\mathbb{R}^{d}\rightarrow \mathbb{R}^n$ durch die
    Abbildungsvorschrift
    \begin{equation}
\begin{aligned}\label{Def:KNN}
  &\mathbf{a}^{[0]}\vcentcolon =\mathbf{x} \\
  &\mathbf{z}^{[l]}\vcentcolon =\mathbf{W}^{[l]}\mathbf{a}^{[l-1]}+\mathbf{b}^{[l]} \\
  &\mathbf{a}^{[l]}\vcentcolon =g^{[l]}(\mathbf{z}^{[l]}) \\
  &f(\mathbf{x}) \vcentcolon = {a}^{[L]}
\end{aligned}
    \end{equation}


für $l=1,\dots,L$.  Gewichtungsmatrizen $\mathbf{W}\in \mathbb{R}^{n_l\times
    n_{l-1}}$, Bias-Vektoren $\mathbf{b}^{[l]}\in\mathbb{R}^{[n_l]}$,
    Aktivierungsvektoren $\mathbf{a}^{[l]}\in\mathbb{R}^{n_l}$,
    Netto-Inputvektoren $\mathbf{z}^{[l]}\in\mathbb{R}^{[n_l]}$ und
    komponentenweise definierte  Aktivierungsfunktionen
    $g^{[l]}:\mathbb{R}^{n_{l-1}}\mapsto \mathbb{R}^{n_l}$. 
    Die verwendete Vektor-Matrix-Multiplikation
    $\mathbf{W}^{[l]}\mathbf{a}^{[l-1]}$ führt dazu, dass jedes Neuron der
    Schicht $l-1$ jedes Neuron der Schicht $l$ beeinflussen kann. Die
    Wirkung der Bias-Vektoren wird deutlich, wenn wir uns an obiges
    Klassifikationsproblem zurückerinnern. Die Bias-Vektoren verschieben die
    Wahrscheinlichkeit eines Neurons in eine der beiden Kategorien
    auszuschlagen - man könnte von einer Art Gewohnheit sprechen.


\section{Minimierungsproblem}
\begin{itemize}
  \item Bisher wurde ein KNN mit Parametern
    $\boldsymbol{\theta}=(\mathbf{W}^{[1]},\mathbf{b}^{[1]},\dots,
    \mathbf{W}^{[L]},\mathbf{b}^{[L]})$ aufgebaut. Um dieses auch nutzen zu
    können, um aus einem Satz von Trainingsdaten Zusammenhänge zu lernen, die
    auf unbekannte Daten angewendet werden können, wird das KNN trainiert.
    Dieses Training meint die Minimierung einer Fehlerfunktion $l:\mathbb{R}^n
    \times \mathbb{R}^n \rightarrow \mathbb{R}$. Diese wird im
    Kontext des maschinellen Lernen häufig Verlustfunktion genannt und wertet
    den Unterschied zwischen den bekannten Daten $\mathbf{y}$ und
    prognostizierten Daten $\hat{\mathbf{y}}$ aus. Diese Verlustfunktion wertet
    den Fehler für ein Trainingsbeispiel aus, um den Fehler für alle
    Trainingsdaten zu minimieren, muss die Summe der Fehler der einzelnen
    Trainingsdaten
    \[
      \min_{\boldsymbol{\theta}\in\Theta}\frac{1}{M}\sum_{i=1}^M
      l(\mathbf{y}^{(i)},\hat{\mathbf{y}}^{(i)})
    \]
    minimiert werden. Die Division mit $M$ ändert die Minimalstelle nicht.

    \smallskip

    Von hier an wird die spezielle Verlustfunktion

    \begin{equation}\label{Eq:l_Min}
    l(\mathbf{y},\hat{\mathbf{y}})=\frac{1}{2}\Vert
    \mathbf{y}-\hat{\mathbf{y}} \Vert^2_2
    \end{equation}

    angenommen. Mit der Festlegung von
    $\hat{\mathbf{y}}=f_{\boldsymbol{\theta}}(\mathbf{x})$ wird das
    Minimierungsproblem (\ref{Eq:l_Min}) zu

    \begin{equation}\label{Eq:KNN_Min}
      \min_{\boldsymbol{\theta}\in\Theta}\frac{1}{2M}\sum_{i=1}^M
      \Vert \mathbf{y}^{(i)}-f_{\boldsymbol{\theta}}(\mathbf{x}^{(i)})
      \Vert_2^2\vcentcolon= \mathcal{J}(\boldsymbol{\theta})
    \end{equation}

    für $(\mathbf{x}^{(i)},\hat{\mathbf{y}}^{(i)})\in \mathcal{S}$.

\end{itemize}

\section{Gradientenverfahren}

	Das Gradientenverfahren ist ein sehr einfaches Verfahren zur Lösung des
    Minimierungsproblem (\ref{Eq:KNN_Min}). 

	Die Idee des Gradientenverfahrens ist es, von einem initialen Wert
    $\boldsymbol{\theta}_0$
    Schrittweise dem Minimum näher zu kommen. Dafür wird in jedem Schritt der
    Gradient der zu minimierenden Funktion an der aktuellen Position ausgewertet
    und ein Schritt mit festgelegter Schrittweite $\lambda > 0$ in Richtung des steilsten
    Abstiegs gemacht. Die Richtung des steilsten Abstiegs entspricht dem
    negativen Gradienten $-\nabla_{\boldsymbol{\theta}}
    \mathcal{J}(\boldsymbol{\theta_t})$ von $\mathcal{J}$ bezüglich
    $\boldsymbol{\theta}$. Mit dieser simplen Anweisung -  Werte den Gradienten aus
    und mache einen Schritt in Richtung des steilsten Abstiegs- kann sich
    dem Minimum genährt werden. Gestoppt wird das Verfahren, sobald der Gradient
    zu Null verschwindet, bzw. die numerische Bedingung $\Vert \nabla
    \mathcal{J}(\boldsymbol{\theta_t})\Vert \leq \epsilon$ mit einem beliebigen, aber
    festen $\epsilon$ erfüllt ist.
    Ob es sich bei dem erreichten Minimum um ein lokales oder globales
    Minimum handelt, kann im Allgemeinen nichts ausgesagt werden. Anders bei
    konvexen Optimierungsproblemen. Bei diesen Problem ist jedes lokale Minimum
    auch ein globales Minimum.

     +++ Kurzer Beweis als Bemerkung++

    Die mathematische Formulierung des obigen Verfahrens ist mit
    \[
    \boldsymbol{\theta}_{t+1} \leftarrow \boldsymbol{\theta}_{t} - \lambda \nabla_{\boldsymbol{\theta}} \mathcal{J}(\boldsymbol{\theta}_{t})
    \]
    gegeben. Der Gradient
    \[
    \nabla_{\boldsymbol{\theta}} \mathcal{J}(\boldsymbol{\theta})=\frac{1}{m}\sum_{i=1}^m \nabla_{\boldsymbol{\theta}}l(\mathbf{y}^{(i)},f(\mathbf{x}^{(i)}))
    \]
   
    von $\mathcal{J}(\boldsymbol{\theta})$ bezüglich $\boldsymbol{\theta}$ wird
    mit der sogenannten Fehlerrückführung (Error-Backpropagation) bestimmt.



\section{Fehlerrückführung}
Fehlerrückführung (engl. Error-Backpropagation) ist ein Algorithmus zur
Bestimmung des für das Gradientenverfahren nötigen Gradienten. Die Idee des
Algorithmus ist, zunächst ein Eingangsdatum durch das Netz zu propagieren, um im
nächsten Schritt die Komponenten des Gradienten durch bereits bekannte Ausdrücke
zu beschreiben. Im Folgenden wird die Fehlerrückführung in vektorieller
Schreibweise für ein Trainingsbeispiel hergeleitet.

\theoremstyle{definition}
\begin{definition}
\[
\delta_i^{[l]}\vcentcolon=\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial z_i^{[l]}}
\]
\end{definition}

\theoremstyle{definition}
\begin{definition}{Fehlervektor}.
Als \textit{Fehlervektor} der $l$-ten Schicht wird 

\[
\boldsymbol{\delta}^{[l]}\vcentcolon = \left(
\begin{matrix}
\delta_1^{[l]} \\
\vdots \\
\delta_{n_l}^{[l]} \\
\end{matrix}\right)
\]
bezeichnet für feste $(\mathbf{x},\mathbf{y})\in\mathbb{R}^{n_0}\times \mathbb{R}^{n_L}$, $l\in{1,\dots,L}$.
\end{definition}

Betrachtet werden nun die Fälle $l<L$ und $l=L$. Mit der Kettenregel folgt für den ersten Fall

\[
\boldsymbol{\delta}^{[L]T}=\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial \mathbf{z}^{[L]}}= \frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial \mathbf{a}^{[L]}} \cdot \frac{\partial \mathbf{a}^{[L]}}{\partial \mathbf{z}^{[L]}} = \left(
\begin{matrix}\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial a_1^{[L]}} & \dots & \frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial a_{n_L}^{[L]}} \end{matrix} \right)
\left( \begin{matrix}
\frac{\partial a_1^{[L]}}{\partial z_1^{[L]}} & \dots & \frac{\partial a_1^{[L]}}{\partial z_{n_L}^{[L]}} \\
\vdots & \ddots & \vdots \\
\frac{\partial a_{n_L}^{[L]}}{\partial z_1^{[L]}} & \dots & \frac{\partial a_{n_L}^{[L]}}{\partial z_{n_L}^{[L]}} \\
\end{matrix} \right)
\]

sowie 

\[
\delta_j^{[l]}= \frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial z_j^{[l]}} = \frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial \mathbf{z}^{[l+1]}} \cdot \frac{\partial \mathbf{z}^{[l+1]}}{\partial z_i^{[l]}}=\sum_{k=1}^{n_{l+1}}\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial z_k^{[l+1]}} \cdot \frac{\partial z_k^{[l+1]}}{\partial z_j^{[l]}}=\sum_{k=1}^{n_{l+1}} \delta_k^{[l+1]} \cdot \frac{\partial z_k^{[l+1]}}{\partial a_j^{[l]}} \cdot \frac{\partial a_j^{[l]}}{\partial z_j^{[l]}} 
\]
für $l>L$.
Mit der getroffenen Annahme über die komponentenweise Definiertheit der Aktivierungsfunktionen folgen die Vereinfachungen

\[
\begin{aligned}
\delta_j^{[l]}=g^{[l]\prime}(z_j^{[l]})\sum_{k=1}^{n_{l+1}}\delta_k^{[l+1]}w_{kj}^{[l+1]}\\
\delta_j^{[L]}=g^{[L]\prime}(z_j^{[L]})\cdot\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial a_j^{[L]}}
\end{aligned}
\]

in Komponentenschreibweise, bzw. die vektoriellen Ausdrücke

\[
\begin{aligned}
\delta_j^{[l]}=g^{[l]\prime}(\mathbf{z}^{[l]})\odot \mathbf{W}^{[l+1]T}\boldsymbol{\delta}^{[l+1]} \\
\delta_j^{[L]}=g^{[L]\prime}(z_j^{[L]})\odot \left[ \frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial \mathbf{a}^{[L]}}\right]^T\\
\end{aligned}
\].

Für den Gradienten $\nabla_{\boldsymbol{\theta}}\mathcal{J}(\boldsymbol{\theta})$ werden nun noch die partiellen Ableitungen

\[
\begin{aligned}
\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial w_{ji}^{[l]}}=\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial z_j^{[l]}} \cdot \frac{\partial z_j^{[l]}}{\partial w_{ji}^{[l]}}=\delta_j^{[l]}a_i^{[l-1]}\\
\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial b_j^{[l]}}=\frac{\partial l(\mathbf{y},f(\mathbf{x}))}{\partial z_j^{[l]}}\cdot \frac{\partial z_j^{[l]}}{\partial b_j^{[l]}}=\delta_j^{[l]} \\
\end{aligned}
\]

der Verlustfunktion bezüglich der Gewichtungsmatrizen und Bias-Vektoren benötigt. In vektorieller Schreibweise folgt daraus für die Komponenten von $\nabla_{\boldsymbol{\theta}}\mathcal{J}(\boldsymbol{\theta})$

\[
\begin{aligned}
\nabla_{\mathbf{W}^{[l]}}l(\mathbf{y},f(\mathbf{x}))=\boldsymbol{\delta}^{[l]}\mathbf{a}^{[l-1]T} & \text{ und }& \nabla_{\mathbf{b}^{[l]}}l(\mathbf{y},f(\mathbf{x}))=\boldsymbol{\delta}^{[l]} \\
\end{aligned}
\]


\section{RMSProp}
 Das RMSProp Verfahren ist eine Modifikation des Gradientenverfahrens mit
Schrittweitensteuerung. Die Einfachheit und Intuitivität des
    Gradientenverfahrens kann in Nähe eines Minimums zu Oszillation führen. Da
    die Schrittweite des Gradientenverfahrens fixiert ist, wird die
    Minimalstelle in vielen Fällen überschritten und ein Schritt des
    Gradientenverfahrens führt durch das Minimum hin zu einem erneuten Entfernen
    von der Minimalstelle. In Extremfällen wird das Minimum nicht erreicht und
    eine beliebige Zahl von Schritten des Gradientenverfahrens führen in der
    Nähe des Minimums.
    Dieses Problem des Gradientenverfahrens versucht das Verfahren RMSProp
    \[
      \begin{aligned}
        \textbf{g} \leftarrow & \nabla \mathcal{J}(\boldsymbol{\theta}) \\
        \textbf{r}\leftarrow & \rho \textbf{r}+(1-\rho)\textbf{g}\odot \textbf{g} \\
        \boldsymbol{\theta}\rightarrow & \boldsymbol{\theta}-\frac{\lambda}{\sqrt{\delta+\textbf{r}}}\odot\textbf{g}
      \end{aligned}
    \]

    mit adaptiver Schrittweitensteuerung zu lösen. Die Konstante $\delta >0$
    verhindert das Verschwinden des Nenners. Der Operator $\odot$ ist
    als komponentenweise Multiplikation zweier Vektoren zu verstehen.
    Die Schrittweitensteuerung dieses Verfahrens berücksichtigt vorherige
    Gradienten. Der Einfluss dieser nimmt wegen $\mathbf{g}\odot \mathbf{g}$
    exponentiell ab. Weiterhin kann über die Konstante $\rho$ gesteuert werden,
    wie stark der Einfluss vorheriger Gradienten ist.

