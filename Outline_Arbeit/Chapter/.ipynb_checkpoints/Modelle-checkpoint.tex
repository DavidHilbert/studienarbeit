\chapter{Modelle}

\section{Kompartimentmodelle}

   Prozesse, die den Transport eines Stoffes beinhalten lassen sich in
    idealisierter Form häufig durch folgendes einfaches Modell beschreiben.
    Zu jedem Zeitpunkt ist die Gesamtmenge eines Stoffes auf $n$ Kompartiments
    $K_1,\dots,K_n$ aufgeteilt. Für jedes Kompartiment kann die zu einem
    Zeitpunkt $t$ beinhaltete Stoffmenge mit $m_i(t)$ angegeben werden.
     Durch irgendeine Art von Mechanismen kann ein Teil der
    Stoffmenge $m_i(t)$ im kleinen Zeitraum $dt$ vom Kompartiment $K_i$ nach $K_j$, $i\neq j$ übergehen.
    Die Menge des Stoffes $k_{ij}m_i(t)dt$ die dabei von $K_i$ nach $K_j$ übergeht ist
    proportional zu der momentanen Stoffmenge $m_i(t)$, dem betrachteten
    Intervall $dt$ und der Proportionalitätskonstanten $k_{ij}\geq 0$.

    Die Stoffmenge, die im Zeitraum von $t$ bis $t+dt$ aus $K_i$ in die anderen
    Kompartiments abfließt ergibt sich aus 

    \[
      \sum^n_{\substack{j=1 \\ j\neq i}}k_{ii}m(t)dt \coloneqq \sum^n_{\substack{j=1 \\ j \neq i}} k_{ij}m_i(t)dt
    \]

    mit der Festlegung 

    \[
      k_{ii} \coloneqq \sum^n_{\substack{j=1 \\ j \neq i}}.
    \]
   
    Der Zufluss nach $K_i$ aus den anderen Kompartiments im gleichen Zeitraum
    kann analog mit

    \[
      \sum^n_{\substack{j=1,j\neq i}}k_{ji}m_j(t)dt
    \]
    angegeben werden. Für die Änderung der Stoffmenge in $K_i$ ergibt sich aus
    der Differenz der Zu-und Abflüsse

    \[
      dm_i(t)=\sum^n_{\substack{j=1 \\ j \neq i}} k_{ji}m_j(t)dt
      - k_{ii}m_i(t)dt.
    \]

    Aus diesem Zusammenhang erhalten wir das System von $n$
    Differentialgleichungen

    \begin{equation}
      \begin{aligned}
        \dot{m}_i=\frac{dm_i(t)}{dt}=\sum^n_{\substack{j=1 \\ j \neq i}} k_{ji}m_j(t)dt
        - k_{ii}m_i(t)dt&,\ i=1,\dots,n. 
      \end{aligned}
    \end{equation}

    In Matrixschreibweise wird dieses System zu
    \[
    \left(
    \begin{matrix}
      \dot{m}_1 \\
      \dot{m}_2 \\
      \vdots \\
      \dot{m}_n \\
    \end{matrix}  
    \right)=
    \left(\begin{matrix}
      -k_{11} & k_{12} &\dots & k_{1n} \\
      k_{21} & -k_{22} & \dots & k_{2n} \\
      \vdots &   \\
      k_{n1} & k_{n2}& \dots & -k_{nn} \\
    \end{matrix}\right)
    \left(
    \begin{matrix}
      m_1(t)\\
      m_2(t)\\
      \vdots \\
      m_n(t)\\
    \end{matrix}
    \right)
    \]

  
   Da innerhalb des Modells die Gesamtmasse erhalten wird, gilt
   $\sum\limits^n_{j=1} k_{ij}=0$ mit $k_{ij}\geq 0$\cite{Heuser95}.

\subsection{Kompartimentmodelle in der Epidemiologie}

\begin{itemize}
	\item Die Epidemiologie beschäftigt sich mit dem Studium des
    Gesundheitszustands einer gesamten Population. Untersuchungsgegenstand der
    Epidemiologie ist neben den gesundheitlichen Folgen auch die Verbreitung von
    Krankheiten aus Bevölkerungsebene. Diese im gewissen Sinne über der akuten
    Erkrankung stehende Perspektive unterscheidet die Epidemiologie deutlich von
    der klinischen Medizin, in deren Fokus der oder die einzelne PatientIn
    steht. 

  \item In der Untersuchung der Verbreitung von Krankheiten sind mathematische Modelle ein
    wichtiges Werkzeug der modernen Epidemiologie. Insbesondere spielen die
    vorher eingeführten Kompartimentmodelle eine bedeutende Rolle. 

  \item Aus der Sichtweise der Epidemiologie kann eine Erkrankung als Abfolge
    von Zuständen, die Teile einer Population annehmen können, betrachtet werden. Diese
    annehmbaren Zustände entsprechen den Kompartiments $K_i$, die Übergänge
    zwischen den Kompartiments Mechanismen wie z.B. Infektions oder Ausheilung.

  \item Im Jahre 1760 hat Daniel Bernoulli ein Kompartimentmodell genutzt, um
    die Auswirkung der Impfung mit dem Kuhpocken-Erreger auf die Ausbreitung der
    Pocken zu untersuchen \cite{Dietz01}. Neben diesem sehr frühen Einsatz haben
    Kompartimentmodelle ihren Nutzen auch in dem Studium der Ausbreitung von HIV
    oder aktuell im Jahre 2020 bei der Ausbreitung von SARS-CoV-2 gezeigt.

  \item Um ein Kompartimentmodell auf eine Population anwenden zu können, müssen
    Kompartiments gefunden werden, die den Krankheitsverlauf angemessen
    beschreiben. Mit der Anzahl der verwendeten Kompartiments geht die Feinheit
    der Betrachtung einher. So lassen sich alle Erkrankungen durch die
    einfachste Unterteilung in Infizierbare ($S$) (von engl. susceptible) und
    Infiziert ($I$) beschreiben. Mit dieser Unterteilung lässt sich die
    generelle Ausbreitung gut beschreiben, jedoch gehen in dieser sehr groben
    Betrachtungsweise Details verloren, die neben der reinen Anzahl der
    Infizierten von Bedeutung sein können (z.B. Anzahl hospitalisierte Infizierte).

  \item Weiter Unterteilungen neben Infizierbaren und Infizierten können nicht
    länger Infizierbare ($R$) (von engl. removed) (z.B. durch Ausheilung,
    Impfung), Verstorbene ($D$) (von engl. dead) oder auch der Erkrankung
    Ausgesetzte, jedoch noch nicht Infizierte ($E$) (von engl. exposed) sein.
    Neben Einführung weiterer Zustände können auch Kompartiments weiter
    unterteilt werden. Ein Beispiel für eine solche feinere Unterteilung ist die
    Unterscheidung Infizierter anhand von Lebensalter.

  \item Epidemiologische Kompartimentmodelle werden nach den genutzten
    Kompartiments und bis zu einem gewisse Grad auch nach den erlaubten
    Übergängen zwischen den Kompartiments benannt. So werden im $SI$-Modell
    Infizierbare und Infizierte betrachtet, im $SIR$-Modell zusätzlich die nicht
    länger Infizierbaren und im $SIS$-Modell wird ein zusätzlicher Mechanismus
    erlaubt, der eine Reinfektion erlaubt.

  \item In Anlehnung an obige Herleitung eines allgemeinen Kompartimentmodells
    wird nun der Spezialfall des $SI$-Modells betrachtet. In diesem Modell
    werden zwei Kompartiments $S$ und $I$ betrachtet mit einem Mechanismus, der
    aus von $S$ nach $I$ führt. Bei diesem Mechanismus handelt es sich um die
    Erkrankung. Zu jedem Zeitpunkt gilt $N=S+I$ mit der Gesamtpopulation $N$. Die entsprechende Konstante wird hier und in allen Modellen, die
    eine Erkrankung betrachtet mit $\alpha$ bezeichnet und bildet die Anzahl der
    Kontakte eines Infizierten ab, die zu einer Infektion führen. Es ist leicht
    vorstellbar, dass $\alpha$ neben der Infektiösität auch die Mobilität und Menge der
    Kontakte eines Individuums beeinflusst wird. Damit ist $\alpha$ keine für eine Krankheit
    spezifische Größe, sondern auch abhängig von soziokulturellen
    Einflüssen. Für Populationen mit ähnlicher kultureller Prägung wird der
    Zahlenwert von $\alpha$ von vergleichbarer Größe sein. 

  \item Da im $SI$-Modell lediglich ein Mechanismus betrachtet wird, der von $S$
    nach $I$ führt, ergibt sich die Änderung des Inhalts beider Kompartiments
    aus dem Abfluss aus $S$, bzw. Zufluss nach $I$. Dieser Fluss hängt ab 
    von der Anzahl der zu einem Zeitpunkt $t$ Infizierbaren $S=S(t)$, der Anzahl
    der aktuell Infizierten $I=I(t)$ und $\alpha$. Konkret kann der Fluss
    angegeben werden durch die Wahrscheinlichkeit, dass ein Infiziertes
    Individuum auf ein infizierbares trifft $\frac{S}{N}$ und der Anzahl der
    Individuen, die durch einen Infizierten infiziert werden $\alpha I$.
    Insgesamt erhalten wir damit für die Änderung von $S$ innerhalb des
    Zeitraums $dt$

    \[
      dS=-\alpha \frac{S}{N} I dt.
    \]
    Für die Änderung von $I$ innerhalb des gleichen Zeitraums erhalten wir

    \[
      dI = \alpha \frac{S}{N} I dt.
    \]

    Aus diesen Zusammenhängen folgt das System von Differentialgleichungen
    
      \begin{align} 
        \dot{S}&=\:-\alpha \frac{SI}{N} \label{Eq:S_SI}\\
        \dot{I}&=\quad  \alpha \frac{SI}{N} \label{Eq:I_SI} \\
        N&= \  S+I \label{Eq:Kont_SI}
      \end{align}

    mit Anfangsbedingungen $I(0)=I_0$ und $S(0)=S_0=N-I_0$.

\begin{figure}
  \centering
\begin{tikzpicture}
  \node[draw, rectangle,minimum size=.5cm] (S) at (0,0) {S};
  \node[draw, rectangle,minimum size=.5cm] (I) at (1.5,0) {I};
  \path 
    (S) edge[->] node[above] {$\alpha$} (I);
\end{tikzpicture}
  \caption{$SI$-Modell}
\end{figure}

\end{itemize}

\subsection{Analytische Lösung des $SI$-Modells}
\begin{itemize}
	\item Die Differentialgleichungen des $SI$-Modells sind nicht-linear und sind
    nicht direkt, z.B. durch Trennung der Veränderlichen möglich. Jedoch können
    sie durch Ausnutzen der Kontinuitätsbedingung (\ref{Eq:Kont_SI}) in die Form einer
    Bernoulli-Differentialgleichung  gebracht werden. Bei
    Bernoulli-Differentialgleichung handelt es sich um Differentialgleichungen
    des Typs

    \[
      y^{\prime}(x)=f(x)y(x)+g(x)y^{\alpha}(x) \quad , \alpha \notin \{0,1\}
    \]

   Differentialgleichungen dieser Form können mit der Festlegung  

    \begin{equation}\label{Eq:Bernoulli_Einsetzen}
      z(x)=y^{1-\alpha}
    \end{equation}
    
    als lineare gewöhnliche Differentialgleichung 
    
    \[
      z^{\prime}(x)=\left( y^{1-\alpha} \right)^{\prime}=(1-\alpha)(f(x)z(x)+g(x))
    \]
  
    aufgefasst werden. In dieser Form kann die Differentialgleichung mit
    üblichen Methoden für lineare gewöhnliche Differentialgleichungen gelöst
    werden.

    Einsetzen von (\ref{Eq:Kont_SI}) in  (\ref{Eq:S_SI}) liefert den
    Zusammenhang

    \[
      \dot{S}=-\frac{\alpha}{N} \left( N-I  \right)
      I=-\alpha\left( I-\frac{I^2}{N}  \right)
    \]

    und führt mit $z(t)=I^{-1}$ auf die inhomogene lineare Differentialgleichung

    \begin{equation} \label{Eq:SI_lin}
      z^{\prime}(t)=-\dot{I}=-\alpha \left( I-\frac{I^2}{N}\right)=-\alpha
      \left(z-\frac{1}{N}\right).
    \end{equation}

    Die Inhomogenität von (\ref{Eq:SI_lin}) erfordert zunächst das Lösen der
    homogenen Differentialgleichung

    \[
      z^{\prime}_{h}(t)+\alpha z_{h}=0
    \]

    welches mit dem Ansatz $z_h(t)=e^{\lambda t}$ zur Lösung 

    \begin{equation}\label{Eq:SI_hom_sol}
      z_h(t)=Ce^{-\alpha t}
    \end{equation}

    führt. Die homogene Lösung einer Differentialgleichung ist das Verhalten,
    das eine Differentialgleichung zeigt, wenn keine äußeren Einfüsse auf das
    System wirken. Wird die Differentialgleichung durch äußere Einflüsse
    (Inhomogenitäten) gestört, so ist die Lösung $y(t)$ der inhomogenen
    Differentialgleichung nach dem Superpositionsprinzip von der Form
    $y(t)=y_h(t)+y_p(t)$ mit der homogenen Lösung $y_h(t)$ und der partikulären
    Lösung $y_p(t)$, die das ungestörte System an die Störung anpasst.

    \smallskip

    Die partikuläre Lösung einer Differentialgleichung mit Veränderung der
    Konstanten gefunden werden. Bei diesem Ansatz wird der konstante Faktor der
    homogenen Lösung als veränderliche Funktion betrachtet und nun zur Lösung
    der inhomogenen Differentialgleichung genutzt. Für (\ref{Eq:SI_hom_sol})
    bedeutet dies konkret

    \[
      z_p(t)=C(t)e^{-\alpha t}.
    \]

    Nach einsetzen in (\ref{Eq:SI_lin}) entsteht

    \begin{equation*}
    \begin{aligned}
      C^{\prime}(t)e^{-\alpha t}-\alpha C(t)e^{-\alpha t}&=-\alpha C(t)e^{-\alpha
      t}+\frac{1}{N} \\
      C^{\prime}(t)&=\frac{\alpha}{N}e^{\alpha t}
    \end{aligned}
    \end{equation*}

    und nach Integration erhalten wir

    \[
      C(t)=\frac{1}{N}e^{\alpha t} \quad ,\tilde{c}\in\mathbb{R}
    \]

    mit bewusster Vernachlässigung der bei der Integration auftretenden
    Konstanten. Der Beitrag der vernachlässigten Konstanten wird durch den
    konstanten Faktor $C$ der homogenen Lösung kompensiert.
    Damit ergibt sich die partikuläre Lösung zu

    \[
      z_p(t)=\frac{1}{N}.
    \]

    \smallskip
    
    Nun, da homogenen und partikuläre Lösung der Differentialgleichung bekannt
    sind, erhalten wir die allgemeine Lösung

    \[
      z(t)=Ce^{-\alpha t}+\frac{1}{N}=\frac{1+NCe^{-\alpha t}}{N}
    \]
    
    und nach Umstellen von (\ref{Eq:Bernoulli_Einsetzen}) 

    \[
      I(t)=\frac{N}{1+NCe^{-\alpha t}}.
    \]

    Die auftretende Konstante kann bestimmt werden durch Einsetzen der
    Anfangsbedingung $I(0)=I_0$

    \begin{equation}
      C=\frac{1}{I_0} -\frac{1}{N}=\frac{N-I_0}{NI_0}
    \end{equation}
    
    was zur Lösung 

    \[
      I(t)=\frac{N}{1+\frac{N-I_0}{I_0}e^{-\alpha t}}
    \]

    führt. Diese Lösung führt zusammen mit (\ref{Eq:Kont_SI}) auf

    \[
      S(t)=N-I(t)=N-\frac{N}{1+\frac{N-I_0}{I_0}e^{-\alpha t}}.
    \]



	\item 
\end{itemize}

\section{Modellfamilie}

\begin{itemize}
	\item Durch Wahl unterschiedlichster Kompartiments und Mechanismen, diese
    verbinden, entstehen verschiedene Modelle, die ihrem Wesen nach ähnlich
    sind. Sie gehören zu einer Modellfamilie. 
    
	\item Ausgehend von dem $SI$-Modell entstehen durch Hinzufügen weiterer
    Kompartiments und einbeziehen weiterer Mechanismen Modelle, die immer mehr
    Aspekte einer Krankheitsausbreitung beschreiben können. Mit Erweiterung der
    Modelle wird es jedoch auch zunehmend schwerer, die Änderungen in den Kompartiments
    auf einzelne Mechanismen zurückzuführen und die Parameter des Modells exakt
    zu bestimmen. Wird lediglich das $SI$-Modell betrachtet, ist jede Änderung
    eindeutig auf den Mechanismus der Erkrankung zurückzuführen. +++ Weiter
    Ausführen+++

  \item 

\end{itemize}

\subsection{Mechanismen}

Grundlegend für die Funktionsweise der betrachteten Modellfamilie sind die
Mechanismen, die den Übergang von einem Zustand in einen anderen erlauben.
Denkbar ist dabei eine Vielzahl möglicher Übergänge zwischen den Kompartiments.
Die Aufgabe des Modellierenden ist es aus dieser großen Menge -sinnvoller als
auch weniger sinnvoller Transitionswegen- eine Anzahl von Mechanismen auszuwählen,
die das Krankheitsgeschehen zu beschreiben vermögen.
Der wohl grundlegendste Mechanismus ist dabei die \textit{Ansteckung}. Dieser
Mechanismus ist sowohl zur Zahl der aktuell Infizierten $(I)$ als auch zur Zahl der
Infizierbaren $(S)$ proportional. Der durch diesen Mechanismus beschriebenen
Übergang von $(S)$ nach $(I)$ wird durch die Größe $\alpha SI$ gesteuert. 
Findet der eben genannte Übergang mit einer zeitlichen Verzögerung statt, wird
ein weiteres Kompartiment $(E)$ als Zwischenspeicher benötigt. Der Übergang nach
$(I)$ findet mit $\beta E$ statt. 
Das Ende der Infektion kann durch zwei mögliche Ausgänge eintreten. Einerseits
kann es zur Genesung und damit Übergang nach $(R)$ kommen, andererseits kann es
zum Tod und Übergang nach $(D)$ kommen. Der Übergang in das Kompartiment der
Verstorbenen findet mit $\mu I$ statt. Betrachtet wird die reine Letalität als 
Verhältnis $\frac{\text{Zahl der Verstorbenen}}{\text{Zahl der Krankheitsausgänge}}$. Aus dem
Kompartiment $(D)$ sind keine weiteren Übergänge möglich. 
Übergang nach $(R)$ erfolgt mit $\gamma I$ und im einfachsten Fall aus $(I)$
heraus. Denkbar sind auch Übergänge aus anderen Kompartiments, wie z.B. der
Übergang von $(S)$ nach $(R)$ durch schützende Impfungen. Mit erreichen von
$(R)$ sind Übergänge in andere Kompartiments möglich. In einigen der
vorliegenden Modellen wird der Übergang von $(R)$ zurück nach $(S)$ mit $\eta R$
betrachtet. Dies ist der Fall, wenn eine überstandene Infektion nicht zu
dauerhafter Immunität führt. 

\begin{table}[h!]\label{Tab_Parameter}
\centering
  \begin{tabular}{p{0.05\textwidth} | p{0.9\textwidth}}

    $\alpha$ & Die  sogenannte Transmissionsrate und beschreibt die durchschnittliche
    Anzahl der Kontakte, bei denen es zu einer Infektion kommen kann. Entsprechend
    ist dieser Parameter abhängig von soziokulturellen Faktoren.\\
    \hline
    $\beta$ & Der Kehrwert dieses Parameters ist die Latenzzeit $\sigma$. Bei diesem
    Parameter handelt es sich um eine für eine Infektion charakteristische Größe. \\
    \hline
    $\gamma$ & Hierbei handelt es sich um die Genesungsrate, deren Kehrwert die
    infektiöse Zeit $\epsilon$ ist. Diese Größe ist ebenfalls typisch für eine
    Erkrankung. \\
    \hline
    $\eta$ & Resultiert eine überstandene Infektion nicht in dauerhafter Immunität,
    so beschreibt $\eta$ die Rate, mit der es zu  einer Reinfektion kommt. \\
    \hline
    $\mu$ & Die für die Erkrankung spezifische Mortalitätsrate.\\

  \end{tabular}
\caption{Beschreibung der in den Modellen verwendeten Parameter}
\end{table}


\begin{itemize}
	\item 
	\item <++>
\end{itemize}

\subsection{Hierarchisierung}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \node (0) at (0,0) {\{$\alpha$\}};
    \node (1) at (-0.8,1) {\{$\alpha$,$\gamma$\}};
    \node (2) at (0.8,1) {\{$\alpha$,$\eta$\}};
    \node (3) at (-1.3,2) {\{$\alpha$,$\gamma$,$\eta$\}};
    \node (4) at (1.3,2) {\{$\alpha$,$\beta$,$\gamma$\}};
    \node (5) at (-1.3,3) {\{$\alpha$,$\beta$,$\gamma$,$\eta$\}};
    \node (6) at (1.3,3) {\{$\alpha$,$\beta$,$\gamma$,$\mu$\}};
    \path
      (0) edge (1)
      (0) edge (2)
      (1) edge (3)
      (1) edge (4)
      (2) edge (4)
      (2) edge (3)
      (3) edge (5)
      (4) edge (5)
      (4) edge (6);
  \end{tikzpicture}
  \caption{Hierarchische Struktur der betrachteten Modelle. Die Ebenen der
  Modellhierarchie ergeben sich aus der Anzahl der notwendigen Parameter eines
  Modells}
  \label{Familie}
\end{figure}
