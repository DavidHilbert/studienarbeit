import numpy as np
import matplotlib.pyplot as plt

def ReLU(x):
    return np.maximum(x,0)

x=np.linspace(-2,2,5)
print(x)
y=ReLU(4*x-4)-ReLU(-x-1)+1
y1=ReLU(4*x-4)
y2=ReLU(-x-1)
print(y)
plt.plot(x,y,label=r'$ReLU(1+ReLU(4x-4)-ReLU(-x-1))$')
plt.plot(x,y1,label=r'$ReLU(4x-4)$')
plt.plot(x,y2,label=r'$ReLU(-x-1)$')
plt.xlabel(r'$x$')
plt.ylabel(r'$y$')
plt.legend(loc=2)
plt.savefig('../Chapter/Images/ReLU_Plateau.png')
plt.show()
