import numpy as np
import scipy as sp
#import tensorflow as tf
#import keras
#from keras.models import Sequential
#from keras.layers import Dense
import matplotlib.pyplot as plt
N=3
y=np.linspace(0,5,5000)
t=np.linspace(0,5,N)

x=[-0.5+0.97*np.exp(t/2) -0.047*np.exp(t) for t in np.linspace(0,5,N)]

plt.plot(t,x,label=r'$f_{\theta}(y)$')
plt.plot(y,np.log(y),label=r'$\frac{\log(y(t))}{t}$')
plt.rc('xtick', labelsize=16)     
plt.rc('ytick', labelsize=16)

plt.ylabel(r'$\alpha_1$')
plt.legend(fontsize=10)
plt.savefig('../Erweitertes_Inhaltsverzeichnis/Images/DGL2_KNN_fun.png')
plt.show()




