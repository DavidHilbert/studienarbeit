import numpy as np
import scipy as sp
from scipy import optimize
import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt

def f_normal(x,a):
    a=np.reshape(a,(4,))
    if x<a[0]:
        return [(a[1]-a[2])*(x-a[0])/(1-a[0])+a[2]]
    else:
        return [(a[3]-a[2])*(x-a[0])/(np.exp(5)-a[0])+a[2]]

def f_dev(x,a):
    a=np.reshape(a,(4,))
    if x<a[0]:
        return [(a[1]-a[2])*(a[0]-1)/(1-x)**2,(x-a[0])/(1-a[0]),(x-1)/(a[0]-1),0]
    else:
        return [(a[3]-a[2])*(a[0]-np.exp(5))/(np.exp(5)-x)**2,0,(x-np.exp(5))/(a[0]-np.exp(5)),(x-a[0])/(np.exp(5)-a[0])]


def Error_grad(a,y,x):
    a=np.reshape(a,(4,))
    z=np.zeros((4,1))
    for i,j in enumerate(y):
        #z+=np.dot((j-f(x[i],a)),np.reshape(f_dev(x[i],a),(1,1)))
        z+=np.dot(np.reshape(f_dev(x[i],a),(4,1)),np.reshape((j-f(x[i],a)),(1,1)))
    return z

def Error_fun(a,y,x):
    z=0
    for i,j in enumerate(x):
        z+=1/2*np.linalg.norm(j-f_normal(y[i],a))**2
    return z

#x,a,y1,ya,y2=np.mgrid[0:15:600,0:np.exp(5):1000,0:5:100,0:5:100,0:5:100]

###reale Werte
a=np.array([50,1,3,5])
#x=np.array([1,1.5,2,2.5,3,3.5,4,4.5,5])
x2=np.linspace(0,5,100)
y2=np.exp(x2)
z=np.log(y2)

#Lade Modell mit 2 Relu
model=Sequential()
#model=keras.models.load_model('../Detailed_View/trained/DGL2_inverse_function_2Neu.h5')
model.add(Dense(2))
model.add(Dense(1))
model.compile(optimizer='rmsprop',loss='MSE')
model.fit(y2,x2,epochs=1000,validation_split=0.3)

#Minimierer
res=sp.optimize.minimize(Error_fun,a,args=(y2,x2))
param=res.x
z2=[f_normal(i,param) for i in y2]

####Gradientenverfahren###
a=np.array([np.exp(2),1,5/2,5])
a=np.reshape(a,(4,1))



def f_grad(x,a):
  if x<a[0,0]:
    return[(a[1,0]-a[2,0])*(x-1)/(1-a[0,0])**2,(x-a[0,0])/(1-a[0,0]),(1-x)/(1-a[0,0]),0]
  elif x>=a[0,0]:
    return[(a[3,0]-a[2,0])*(x-np.exp(5))/(np.exp(5)-a[0,0])**2,0,(np.exp(5)-x)/(np.exp(5)-a[0,0]),(x-a[0,0])/(np.exp(5)-a[0,0])]

def f(x,a):
  if x<a[0,0]:
    return (a[1,0]-a[2,0])/(1-a[0,0])*(x-a[0,0])+a[2,0]
  elif x>=a[0,0]:
    return (a[3,0]-a[2,0])/(np.exp(5)-a[0,0])*(x-a[0,0])+a[2,0]

x=np.linspace(0,5,100)
y=np.exp(x)
z=np.zeros((4,1))

def Fehler_grad(x,y,a): 
  z=np.zeros((4,1))
  for count,val in enumerate(y):
    z+=np.dot(np.reshape(f_grad(val,a),(4,1)),(x[count]-f(val,a)))
  return z

i=0
while np.linalg.norm(Fehler_grad(x,y,a))>0.00001:
  i+=1
  if i%500==0:
    print(np.linalg.norm(Fehler_grad(x,y,a)))
  a=a+0.01*np.reshape(Fehler_grad(x,y,a),(4,1))
  
print(a)
###Ende Gradientenverfahren###

param_grad=np.array([i for i in a[:,0]])
param_grad=np.reshape(param_grad,(4,1))
z_grad=[f(i,param_grad) for i in y2]

###Plotten###var mit 2 sind aus altem Code mit dem KNN und Minimierungsverfahren
#geplottet werden
plt.plot(y2,x2,label=r'$\ln(x)$')
plt.plot(y2,z2,label='Optimierungsverfahren')
plt.plot(y2,z_grad,label='Gradienten')
plt.plot(y2,model.predict(y2),label='KNN')
plt.legend()
plt.show()
