import random as rnd
import numpy as np
import scipy as sp
import tensorflow as tf
from keras.layers import Dense, Flatten
from keras.models import Sequential
import keras
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import solve_ivp



def scaled_sigmoid(x):
    return tf.math.sigmoid(x)*5;

def ReLU_vec(x):
    for i in np.size(x):
        if x[i]<0:
            x[i]=0
        else:
            x[i]=x[i]
    return x

def ReLU(x):
    return np.maximum(x,0)

def KNN_output(x,W1,b1,W2,b2):
    return ReLU(np.dot(ReLU(np.dot(W1,x)+b1),W2)+b2)
    

data_out=np.load('./parameters_DGL2_system_2Komp.npy')
data_in=np.load('./solutions_DGL2_system_2Komp.npy')
#data_out=np.load('./parameters_DGL2_system_SI_SIR_SEIR.npy')
#data_in=np.load('./solutions_DGL2_system_SI_SIR_SEIR.npy')

#data_in=np.load('./solutions_DGL2_system_2Komp.npy')

#data_in=np.reshape(data_in_orig,(10000,-1))

data_in_scaled=np.zeros((np.shape(data_in)[0],6))


#,data_in[:,0,2],data_in[:,1,2],data_in[:,2,2],data_in[:,3,2],1/data_in[:,2,2],data_in[:,0,3]/(data_in[:,0,2]*data_in[:,2,2]),data_in[:,3,2]/data_in[:,2,2],data_in[:,3,3]/data_in[:,3,2],data_in[:,1,3]/data_in[:,1,2],data_in[:,0,2]/data_in[:,1,2],data_in[:,0,3]/data_in[:,0,2]])


data_in_scaled[:,0]=np.ones(np.shape(data_in)[0])
#data_in_scaled[:,1]=data_in[:,0,2]
#data_in_scaled[:,2]=data_in[:,1,2]
#data_in_scaled[:,3]=data_in[:,2,2]
#data_in_scaled[:,4]=data_in[:,3,2]
#data_in_scaled[:,5]=1/data_in[:,2,2]
#data_in_scaled[:,6]=data_in[:,0,3]/(data_in[:,0,2]*data_in[:,2,2])
#data_in_scaled[:,7]=data_in[:,3,2]/data_in[:,2,2]
#data_in_scaled[:,8]=data_in[:,3,3]/data_in[:,2,2]
#data_in_scaled[:,9]=data_in[:,1,3,]/data_in[:,1,2]
#data_in_scaled[:,10]=data_in[:,0,2]/data_in[:,1,2]
#data_in_scaled[:,11]=data_in[:,0,3]/data_in[:,1,2]

#data_in_scaled[:,1]=data_in[:,2,2]
#data_in_scaled[:,2]=data_in[:,3,2]
#data_in_scaled[:,3]=1/data_in[:,2,2]
#data_in_scaled[:,4]=data_in[:,3,2]/data_in[:,2,2]
#data_in_scaled[:,5]=data_in[:,3,3]/data_in[:,2,2]

num_neu=1
model=Sequential()
initializer = tf.keras.initializers.HeNormal()
model.add(Flatten())
model.add(Dense(num_neu,activation='relu'))#,kernel_initializer=initializer))
model.add(Dense(1,activation='relu'))#,kernel_initializer=initializer))
model.compile(optimizer=keras.optimizers.RMSprop(),loss='mse') 
model.fit(data_in,data_out,epochs=100,validation_split=0.3)

print(model.trainable_weights)


#model.save('./trained/DGL2_inverse_function_1000Neu.h5')


var=[v.numpy() for v in model.trainable_variables]

N=1000000
z=np.zeros((10000,2,5))
alpha=np.linspace(-5,0,10000)
z[:,0,0]=(N-1)/N
z[:,0,0]=(N-N/(1+(N-1)*np.exp(0*alpha)))/N
z[:,0,1]=(N-N/(1+(N-1)*np.exp(2.5*alpha)))/N
z[:,0,2]=(N-N/(1+(N-1)*np.exp(5*alpha)))/N
z[:,0,3]=(N-N/(1+(N-1)*np.exp(7.5*alpha)))/N
z[:,0,4]=(N-N/(1+(N-1)*np.exp(10*alpha)))/N
z[:,1,0]=(1)/N
z[:,1,1]=(N-z[:,0,1])/N
z[:,1,1]=(N-z[:,0,1])/N
z[:,1,2]=(N-z[:,0,2])/N
z[:,1,3]=(N-z[:,0,3])/N
z[:,1,4]=(N-z[:,0,4])/N


#data_iter=data_in[:,:,3]
z1=[ReLU(np.add(np.dot(var[0].transpose(),np.reshape(i,(-1,1))),np.reshape(var[1],(-1,1)))) for i in data_in]
z1=np.reshape(z1,(-1,num_neu))
print(z1[:10,0])
z1=np.array(z1)

fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(data_in[:9999,0,3],data_in[:9999,1,3],z1[:9999,0])
#ax.scatter(z[:,0,3],z[:,1,3],z1[:,1],label='ReLU2')
#ax.scatter(z[:,0,3],z[:,1,3],z1[:,2],label='ReLU3')
#ax.scatter(z[:,0,3],z[:,1,3],z1[:,3],label='ReLU4')
#ax.scatter(z[:,0,3],z[:,1,3],z1[:,4],label='ReLU5')
#ax.scatter(data_in[:,0,2],data_in[:,1,2],data_out[:,0],label='orig')
#ax.scatter(data_in[:,0,2],data_in[:,1,2],model.predict(data_in[:,:,3]),label='KNN')
plt.savefig('../Outline_Arbeit/Chapter/Images/2Komp_alpha_invers_feste0_ReLU3D.png')
plt.legend()
plt.show()

plt.scatter(data_in[:9999,0,3],z1[:9999,0])
plt.show()

##x,y=np.meshgrid(x,y)
#fun=[KNN_output(x,var[0].transpose(),np.reshape(var[1],(-1,1)),var[2].transpose(),np.reshape(var[3],(-1,1)))]# for i,val in enumerate(x[0,0,:])]
#print(np.shape(fun))
#fun=np.reshape(fun,np.shape(x[:]))
##
##plt.plot(x,np.log(x),label='ln(x)')
#plt.plot(x,model.predict(x),label='model_pred')
#plt.scatter(data_in,data_out)
#plt.legend()
#plt.show()


#plt.scatter(data_in[:,0,0],model.predict(data_in),label='1')
#plt.scatter(data_in[:,0,1],model.predict(data_in),label='2')
#plt.scatter(data_in[:,0,2],model.predict(data_in),label='3')
#plt.scatter(data_in[:,0,3],model.predict(data_in),label='4')
#plt.scatter(data_in[:,0,4],model.predict(data_in),label='5')
#plt.scatter(data_in[:,0,0],data_out,label='1o')
#plt.scatter(data_in[:,0,1],data_out,label='2o')
#plt.scatter(data_in[:,0,2],data_out,label='3o')
#plt.scatter(data_in[:,0,3],data_out,label='4o')
#plt.scatter(data_in[:,0,4],data_out,label='5o')
#plt.xlabel(r'$\mathrm{u}_2$',fontsize=14)
#plt.ylabel(r'$\alpha$',fontsize=14)
#plt.legend()
#plt.yticks(fontsize=14)
#plt.xticks(fontsize=14)
#plt.savefig('../Outline_Arbeit/Chapter/Images/2Komp_alpha_invers_KNN_2Neuronen_feste0.png')

#plt.scatter(data_in[:,1,2],model.predict(data_in),label='v')
#plt.scatter(data_in[:,2,2],model.predict(data_in)[:,0],label='w')
plt.figure()
plt.scatter(data_in[:9999,0,3],data_out[:9999,0])
plt.yticks(fontsize=14)
plt.xticks(fontsize=14)
plt.xlabel(r'$\mathrm{u}_2$',fontsize=14)
plt.ylabel(r'$\alpha$',fontsize=14)
plt.savefig('../Outline_Arbeit/Chapter/Images/2Komp_alpha_invers_feste0.png')
plt.legend()
plt.show()

#plt.scatter(data_in_scaled[:,0,2],model.predict(data_in_scaled),label='u')
#plt.scatter(data_in_scaled[:,1,2],model.predict(data_in),label='v')
##plt.scatter(data_in[:,2,2],model.predict(data_in)[:,0],label='w')
#plt.scatter(data_in_scaled[:,0,2],data_out_scaled[:,0],label='orig u')
#plt.scatter(data_in_scaled[:,1,2],data_out_scaled[:,0],label='orig v')
#plt.legend()
#plt.show()

print("1")
print(data_out[2,:])
print(model.predict(np.reshape(data_in[2,:,:],(1,2,-1))))
print("2")
#print(data_in[7539,:,:])
print(data_out[75,:])
print(model.predict(np.reshape(data_in[75,:,:],(1,2,-1))))
print("3")
#print(data_in[3728,:,:])
print(data_out[3728,:])
print(model.predict(np.reshape(data_in[3728,:,:],(1,2,-1))))
print("4")
#print(data_in[13,:,:])
print(data_out[13,:])
print(model.predict(np.reshape(data_in[13,:,:],(1,2,-1))))
print("5")
#print(data_in[1649,:,:])
print(data_out[1649,:])
print(model.predict(np.reshape(data_in[1649,:,:],(1,2,-1))))
#model2.save('./trained/model_demo_no_hidden.h5')
