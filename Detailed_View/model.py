import numpy as np
import scipy as sp
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import random as rnd

def model(t,x,alpha):
    return [-alpha*x[0]*x[1],alpha*x[0]*x[1]]
#def model(t,x,a,b):
#    return [-a*x[0]*x[1],a*x[0]*x[1]-b*x[1],b*x[1]]
def model_SI(t,x,a,b,c):
    return [-a*x[0]*x[1],0,a*x[0]*x[1],0]
def model_SIR(t,x,a,b,c):
    return [-a*x[0]*x[1],0,a*x[0]*x[1]-c*x[1],c*x[2]]
def model_SEIRS(t,x,a,b,c,d):
    return [-a*x[0]*x[2]+d*x[3],a*x[0]*x[2]-b*x[1],b*x[1]-c*x[2],c*x[2]-d*x[3]]

size=10000
T=10
num_points=5
t=np.linspace(0,T,num_points)
print(t)

list_sol=np.zeros((size,2,num_points))
list_alpha=np.zeros((size,1))

N=1000000
S0=(N-1)/N
I0=1/N
E0=0
R0=0

masken=np.zeros((3,3))
masken[:,0]=np.array([1,0,0])
masken[:,1]=np.array([1,0,1])
masken[:,2]=np.array([1,1,1])

print(masken)

plt.ion()
a=np.zeros((4,))
for i in range(0,size):
    c=rnd.random()*50
    S0=(N-c)/N
    I0=c/N
    a[0]=rnd.random()*5
    a[1]=rnd.random()
    a[2]=rnd.random()
    a[3]=rnd.random()*0.1
#    mask=rnd.choice(np.transpose(masken))
#    a=a*mask
#
#    if np.linalg.norm(a,ord=0)==1:
#        sol=solve_ivp(model_SI,[0,T],[S0,E0,I0,R0],args=(a[0],a[1],a[2]),t_eval=t)
#    if np.linalg.norm(a,ord=0)==2:
#        sol=solve_ivp(model_SIR,[0,T],[S0,E0,I0,R0],args=(a[0],a[1],a[2]),t_eval=t)
#    else:
#        sol=solve_ivp(model_SEIR,[0,T],[S0,E0,I0,R0],args=(a[0],a[1],a[2]),t_eval=t)
    sol=solve_ivp(model,[0,T],[S0,I0],args=(a[0],),t_eval=t)
    print(i)
    list_sol[i,:,:]=sol.y
    list_alpha[i,:]=np.transpose(a[0])
    #list_alpha[i,:]=np.transpose([a[0],a[1]])
    #list_alpha[i,:]=np.transpose(a)
    if i%100==0:
        plt.plot(sol.y[0],label='x1') 
        plt.plot(sol.y[1],label='x2') 
        #plt.plot(sol.y[2],label='x3') 
        #plt.plot(sol.y[3],label='x4') 
        #plt.legend()
        plt.pause(0.5)
        plt.show()
    #list_sol[i,:]=sol.y
    #list_alpha[i,:]=a[0]
    


np.save('./solutions_DGL2_system_2Komp_Initial.npy',list_sol)
np.save('./parameters_DGL2_system_2Komp_Initial.npy',list_alpha)
print('fertig')
