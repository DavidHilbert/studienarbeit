import numpy as np
import scipy as sp
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import random as rnd

def model(t,x,alpha):
    return [alpha,]

size=10000
T=10
num_points=2
t=np.linspace(0,T,num_points)
print(t)
list_sol=np.zeros((size,1,num_points))
list_alpha=np.zeros((size,2))

plt.ioff()
for i in range(0,size-1):
    alpha=rnd.random()*10
    beta=rnd.random()*10
    #sol=solve_ivp(model,[0,T],[0],args=(alpha,beta),t_eval=t)
    sol=solve_ivp(model,[0,T],[0],args=(alpha,),t_eval=t)
    list_sol[i,:]=[sol.y[0]]
    #list_alpha[i,:]=np.transpose([alpha,beta])
    list_alpha[i,:]=np.transpose([alpha])
np.save('./solutions.npy',list_sol)
np.save('./parameters.npy',list_alpha)
