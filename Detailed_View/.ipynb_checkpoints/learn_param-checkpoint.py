import sys
import numpy as np
import scipy as sp
import tensorflow as tf
from keras.layers import Dense
from keras.models import Sequential
import keras
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

t=np.linspace(0,1,10) 
x1=np.exp(0.77*t)
x2=np.exp(0.83*t)

#plt.plot(t,x1,label='.77')
#plt.plot(t,x2,label='.83')
#plt.legend()
#plt.show()

def scaled_sigmoid(x):
    return tf.math.sigmoid(x)*5;

def lin_model(t,x,alpha):
    return [alpha]

def mse_mod(y_true,y_pred):
    t=np.linspace(0,10,2)
    print('Hello')
    tf.shape(y_pred)
    #tf.print(y_pred,output_stream=sys.stdout)
    print('Hello2')
    #sol=solve_ivp(lin_model,[0,10],[0],t_eval=t,args=(y_pred,))
    cost=tf.reduce_sum(tf.square(y_true - solve_ivp(lin_model,[0,10],[0],t_eval=t,args=(y_pred,)).y[0]))
    return cost

data_out=np.load('./parameters.npy')
data_in=np.load('./solutions.npy')


input_shape=np.shape(data_in)

model=Sequential()
#model.add(Dense(500,activation='relu'))
#model.add(Dense(2,activation='sigmoid'))
model.add(Dense(1,activation='relu'))
model.compile(optimizer='rmsprop',loss=mse_mod,metrics=['accuracy']) 

model.fit(data_in,data_out,epochs=30, validation_split=0.3)
print("1")
print(data_in[2,:,:])
print(data_out[2])
print(model.predict(data_in[2,:,:]))
print("2")
print(data_in[7539,:,:])
print(data_out[7539])
print(model.predict(data_in[7539,:,:]))
print("3")
print(data_in[3728,:,:])
print(data_out[3728])
print(model.predict(data_in[3728,:,:]))
print("4")
print(data_in[13,:,:])
print(data_out[13])
print(model.predict(data_in[13,:,:]))
print("5")
print(data_in[1649,:,:])
print(data_out[1649])
print(model.predict(data_in[1649,:,:]))
model.save('./trained/model_lin_red.h5')
print(model.summary())
print(model.trainable_weights)
print("fertig")
