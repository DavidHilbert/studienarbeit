import numpy as np
import scipy as sp
from mpl_toolkits import mplot3d
#import tensorflow as tf
#import keras
#from keras.models import Sequential
#from keras.layers import Dense
import matplotlib.pyplot as plt

def min_fun(x,a,w1,w2,b1,b2):
    res=0
    for counter,val in enumerate(x):
        res+=1/2*(a[counter,0]-ReLU(w2[0,0]*ReLU(w1[0,0]*x[counter,0]+b1[0,0])+w2[0,1]*ReLU(w1[1,0]*x[counter,0]+b1[1,0])+b2))**2
    return res

def ReLU(x):
    res=np.maximum(0,x)
    return res

x=np.load('./solutions_DGL2_Untersuchung.npy')
#T=500
#x=np.linspace(0,np.exp(5),T)
#x=np.reshape(x,(T,1)) 
x=np.reshape(x[:,1],(10000,1))
alpha=np.load('./parameters_DGL2_Untersuchung.npy')

#Optimal weights
w1_opti=np.array([0.91748923, 0.385246])
w1_opti=np.reshape(w1_opti,(2,1))

w2_opti=np.array([0.24210005,-0.53091383])
w2_opti=np.reshape(w2_opti,(1,2))

b1_opti=np.array([0.18759958,-4.7118144])
b1_opti=np.reshape(b1_opti,(2,1))

b2_opti=np.array([0.20931938])
b2_opti=np.reshape(b2_opti,(1,1))


num_points=100
w1=np.linspace(0,0.5,num_points)
w2=np.linspace(-5,-4,num_points)
w1,w2=np.meshgrid(w1,w2)

#z=np.zeros(np.shape(w1))
#for j in range(0,np.shape(w1)[0]):
#    print(j)
#    for i in range(0,np.shape(w1)[1]):
#        w=np.array([w1[j,i],w2[j,i]])
#        w=np.reshape(w,(2,1))
#        z[j,i]=min_fun(x,alpha,w1_opti,w2_opti,b1_opti,w)

b2=np.linspace(0,0.5,num_points)
z=[min_fun(x,alpha,w1_opti,w2_opti,b1_opti,i) for i in b2]
print(z)
z=np.array(z)
z=np.reshape(z,(num_points,1))
print(np.shape(z))
#minimalstelle=np.min(z)
i,j = np.unravel_index(np.argmin(z), z.shape)

fig=plt.figure()
#ax=plt.axes(projection='3d')
plt.rc('xtick', labelsize=14)
plt.rc('ytick', labelsize=14)
#ax.xaxis.set_tick_params(labelsize='large')
#ax.yaxis.set_tick_params(labelsize='large')
#ax.zaxis.set_tick_params(labelsize='large')
#ax.plot_surface(w1,w2,z)
plt.plot(np.array(b2),z)
plt.xlabel(r'$\mathbf{b}^{(1)}_1$')
#plt.ylabel(r'$\mathbf{b}^{(1)}_2$')
plt.show()

plt.figure()
plt.rc('xtick', labelsize=14)     
plt.rc('ytick', labelsize=14)
contours=plt.contour(w1,w2,z)
#ax=plt.axes(projection='3d')
ax.xaxis.set_tick_params(labelsize='large')
plt.xlabel('W',fontsize=14) 
plt.ylabel('b',fontsize=14) 
plt.clabel(contours,fmt='%2.0d',colors='k',fontsize=14)
#ax.set_label('Z') 
plt.show()
