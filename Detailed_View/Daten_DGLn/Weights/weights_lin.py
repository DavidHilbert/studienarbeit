import numpy as np
import scipy as sp
import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt

model=Sequential()
model=keras.models.load_model('./trained/model_quad_red.h5')
np.savetxt('Weights_quad_red.csv',model.get_weights()[0], delimiter=",")
np.savetxt('Bias_quad_red.csv',model.get_weights()[1], delimiter=",")
