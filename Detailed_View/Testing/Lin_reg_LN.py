import numpy as np
import scipy as sp
import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"
import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt

x=np.array([1,5,10])
y=np.log(x)

z=0.248*x-0.021

model=Sequential()
#model.add(Dense(2,activation='relu'))
model.add(Dense(1,activation='relu'))
model.compile(optimizer='rmsprop',loss='mse')

model.fit(x,y,epochs=500)
print(model.trainable_weights)

z2=model.predict(x)

plt.scatter(x,y)
plt.plot(x,z,label='Lin Reg')
plt.plot(x,z2,label='Predicted')
#plt.plot(x,y,style='scatter')
plt.legend()
plt.show()
