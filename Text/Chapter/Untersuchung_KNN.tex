\chapter{Untersuchung des Verhaltens neuronaler Netze}

Im Rahmen der folgenden Untersuchungen werden vektorwertige Funktionen 
$$
\textbf{y}=\left( \begin{matrix} 
y_1 \\
\vdots \\
y_d \\
\end{matrix} \right)
$$

betrachtet. Diese seien Lösungen einer Differentialgleichung (DGL) vom Typ

\[
\dot{\textbf{y}}=g(t,\textbf{y};a), \ \textbf{y}=\textbf{y}_0
\]

mit stetiger rechter Seite $g(t,y;a)$, bekannter Anfangsbedingung $\textbf{y}_0$ und Parametern $a\in\mathbb{R}^k$.
Für die Parameter $a$ sei
\begin{equation}\label{Eq:Forward} a\rightarrow \textbf{y}\in C([0,T])\end{equation}
erfüllt. 
Ist $\textbf{y}$ nur zu Zeitpunkten $t_n, \ n=1,\dots,N$ bekannt, so wird (\ref{Eq:Forward}) zu dem diskreten Problem 

\begin{equation}\label{Eq:Forward_Disk}
a \rightarrow \vec{y} \in \mathbb{R}^N
\end{equation}
für $\vec{y}=\left(
\begin{matrix} \textbf{y}^{(1)} & \dots & \textbf{y}^{(N)} \\ \end{matrix}
\right)^T$ mit $\textbf{y}^{(n)}=\textbf{t}_n$.

An dieser Stelle ist das Parameteridentifikations-Problem von Interesse. Formell
geht damit die Suche nach einer Funktion

\[
  f:\vec{y}\rightarrow a
\]
einher. Dies ist das zu (\ref{Eq:Forward_Disk}) inverse Problem. Die Idee des
maschinellen Lernens mit neuronalen Netzen ist eine Funktion $f\in K$ zu finden, die für
die $M$ gegebene Trainingsbeispiel 

\[
  (a^{(m)},\vec{y}^{(m)})\in\mathbb{R}^k \times \mathbb{R}^{N},\ m=1,\dots ,M
\]

die Fehlerfunktion 

\begin{equation}\label{Eq:Fehlerfun_Disk}\mathcal{J}=\frac{1}{2}\sum_{m=1}^{M}\Vert f(\vec{y}^{(m)})-a^{(m)} \Vert
_2^2
\end{equation}

minimiert. Die Funktionenklasse $K$ wird dabei durch die Architektur des
verwendeten neuronalen Netzes bestimmt. 
 Für ein simples neuronales Netz mit einem Inputwert $x$ und einem Ausgangswert
 $y$ und Aktivierungsfunktion $ReLU(x)=max(0,x)$ sind die möglichen
 durch dieses neuronale Netz darstellbaren Funktionen von der Form 

  \[
    f(x)=\begin{cases} mx+b &, mx+b \geq 0\\ \qquad0&, \text{sonst.} \end{cases}
  \]
  mit $m,b\in \mathbb{R}$. Für den Fall $mx+b\geq 0$ entspricht die Minimierung
  von (\ref{Eq:Fehlerfun_Disk}) einer linearen Regression.\linebreak
  Von Interesse ist die Klasse der darstellbaren Funktionen $K$ für jene
  neuronale Netze mit Tiefe $L$ und Schichtbreite $n_l>1,\ l=1,\dots,L$
  (Notation vgl. (\ref{Def:KNN})).
 Wird obiges Minimalbeispiel eines neuronalen Netzes um eine verdeckte Schichte
 ergänzt, wird $K$ zu
  \begin{figure}[H]
    \[
      K=\left\{
        f\in C([0,T]),f^{\prime}\in C([0,T]) \text{ f.ü.} \mid
        f(x)=ReLU(\textbf{W}^{[2]}ReLU(\textbf{W}^{[1]}x+\textbf{b}^{[1]})+\textbf{b}^{[2]})
        \right\}    \] 
    \caption{Das geht bestimmt eleganter}
  \end{figure}
 mit $\textbf{W}^{[l]}\in \mathbb{R}^{n_{l}\times n_{l-1}},\textbf{b}^{[l]}\in
 \mathbb{R}^{n_l}$.
 Dies sind stückweise lineare Funktionen $f:\mathbb{R}\rightarrow \mathbb{R}^+$.
 Die Breite der verdeckten Schicht entspricht dabei der Anzahl linearer Teilstücke.

\begin{Beispiel} 

  Für ein neuronales Netz mit zwei verdeckten Neuronen (Abbildung
  (\ref{Fig:Beispiel_zwei_Neuronen})) ist die das neuronale Netz beschreibende
  Funktion von der Form 

  \[
    f(x)=ReLU(W_1^2ReLU(W_1^1x+b_1^1)+W_2^2ReLU(W_2^1x+b_2^1)+b_1^2).
  \]
  
  Darstellbar für ein solches neuronales Netz sind Funktionen, die sich aus
  einer gewichteten Summe zweier $ReLU$-Funktionen erzeugen lassen. 


\begin{figure}[H]
\begin{center}
  \begin{tikzpicture}[node distance=1.5cm and 5cm, auto]
    \node[draw, circle, minimum size=.7cm] (1) at (0,0) {y};
    \node[draw, circle, minimum size=.7cm, above right of=1] (2) {};
    \node[draw, circle, minimum size=.7cm,below right of=1] (3) {};
    \node[draw, circle, minimum size=.7cm,below right of=2] (4) {a};
  \path
    (1) edge node[scale=0.8]{}(2)
    (1) edge node {}(3)
    (2) edge node {}(4)
    (3) edge node {}(4);
\end{tikzpicture}
\end{center}
  \caption{Neuronales Netz mit zwei verdeckten Neuronen}
  \label{Fig:Beispiel_zwei_Neuronen}
\end{figure}

\end{Beispiel}

Nun zu einem ersten Parameteridentifikations-Problem für die DGL

\begin{equation}\label{Eq:DGL1}
  \begin{aligned}
    y^{\prime}=a&,\ y_0=0&,\ t\in[0,10].
  \end{aligned}
\end{equation}

Analytisch kann dieses Problem durch Trennung der Veränderlichen gelöst werden

\[
  \begin{aligned}
    \frac{dy}{dt}&=a\\
    y(t)&=\int a dt + c &,c\in \mathbb{R}\\
  \end{aligned}
\]

unter Ausnutzung der Anfangsbedingung kann die Lösung $y(t)=a t$ gefunden
werden. Mit dieser bekannte Lösung lässt sich leicht eine Funktion
$f:y(t)\rightarrow a$ finden. Für ein beliebiges $y(T)=y_N$ kann der Parameter
$a$ durch 
\[
  a\coloneqq f(y)=\frac{y}{t}
\]
bestimmt werden. Numerisch kann die Bestimmung von $a$ über die Betrachtung des
Differenzenquotienten erfolgen. Dafür wird zunächst die Taylor-Reihe von
$y(t)$ um den Entwicklungspunkt $t$ betrachtet

\[
  y_{i+1}\coloneqq y(t+h)=y(t)+y^{\prime}(t)h
\]

und anschließend durch einsetzen in (\ref{Eq:DGL1}) der Differenzenquotient

\[y^{\prime}=\frac{y_{i+1}-y_i}{h} =a\]
erzeugt. Das erste Gleichheitszeichen ergibt sich, $\frac{d^k
y}{dt^k}=0,\text{ für }k>1$ gilt. Mit Kenntnis über zwei Punkten auf $y(t)$ und
$h$ kann $a$ so exakt bestimmt werden. Mit Wahl von $y_i=y_0=0$ und
$y_{i+1}=y(10)$ gilt $h=10$ und damit 

\[
  a=\frac{y_{i+1}}{10}.
\]

Neben der analytischen und der numerischen Herangehensweise kann dieses Problem
auch mit den Methoden des maschinellen Lernens gelöst werden. 





















\begin{comment}
Im Rahmen der folgenden Untersuchungen werden Differentialgleichungen (DGLen)
vom Typ
\begin{equation}\label{Eq:DGL_gen}
\dot{y}(t)=h(t;\textbf{a}),\  t\in [0,T]
\end{equation}

mit $\boldsymbol{\alpha}=\left(
  \begin{matrix}
    a_1, & \dots & ,\ a_n
  \end{matrix}
    \right)^T$
betrachtet. Gesucht ist eine Funktion $g(y):\mathbb{R}^N \rightarrow
\mathbb{R}^n, y(t)\rightarrow \textbf{a}$, die Parameter $\textbf{a}$ zu
(\ref{Eq:DGL_gen}) in Abhängigkeit von der Lösung $y(t)$ beschreibt. Minimiert wird nun das
Fehlerfunktional 
\[
  \mathcal{J}=\Vert \textbf{a} - g(y) \Vert^2_2.
\]

Der Übergang zur diskreten Formulierung findet durch die Wahl von $N$ Stützpunkten
$$\tilde{\textbf{y}}\coloneqq \textbf{y}(\boldsymbol{\tau})= \left(
            \begin{matrix}
              y_0=y(\tau_0)\\
              \vdots \\
              y_{N-1}=y(\tau_{N-1})\\
            \end{matrix}
          \right)$$
          für äquidistante Zeitpunkte
$\boldsymbol{\tau}=\left[ \begin{matrix}
      \tau_0=0 \leq & \tau_i & \leq \tau_{N-1}=L
      \end{matrix}
      \right]$
      statt. Als Näherung von $g(y)$ wird die Funktion $\tilde{g}(\tilde{\textbf{y}}):\mathbb{R}^N
    \rightarrow \mathbb{R}^{n},\ \tilde{\textbf{y}} \rightarrow
    \boldsymbol{\alpha}$ gesucht.
    Im Kontext des maschinellen Lernens mit neuronalen Netzen wird die
    Bezeichnung $\hat{\textbf{y}}^{(i)}\coloneqq f_{\boldsymbol{\theta}}(\tilde{\textbf{y}}^{(i)})=\tilde{g}(\tilde{\textbf{y}}^{(i)})$
    eingeführt und das angepasste Minimierungsproblem (\ref{Eq:KNN_Min})

    \[
      \min_{\boldsymbol{\theta}}\hat{\mathcal{J}}\coloneqq\sum_{i=1}^{m} \Vert
    \boldsymbol{\alpha}^{(i)}-\hat{\textbf{y}}^{(i)} \Vert _2^2
    \]

    für Trainingsbeispiele aus der Menge $\mathcal{S}=\{
      (\tilde{\mathbf{y}}^{(1)},\boldsymbol{\alpha}^{(1)}),\dots
      , (\tilde{\textbf{y}}^{(m)},\boldsymbol{\alpha}^{(m)})\}$ gelöst.


Gelöst wird nun das angepasste  Minimierungsproblem (\ref{Eq:KNN_Min}) 

\[
  \min_{\boldsymbol{\theta}}\sum_{i=1}^{m} \Vert
  \boldsymbol{\alpha}^{(i)}-\hat{\textbf{y}}^{(i)} \Vert _2^2
  \]


\section{Methodik}

Um das Verhalten neuronaler Netze für das Parameteridentifikations-Problem für
Differentialgleichungen (DGLen) zu untersuchen, wird auf neuronale Netze ohne
verdeckte Schicht - sogenannte Perzeptrons- zurückgegriffen. Jedes neuronale
Netz wird dabei mehrfach trainiert und jeweils der Durchlauf gewählt dessen
Erwartungswert am wenigsten vom realen Erwartungswert abweicht.
Im Fokus steht dabei die Frage wie ein neuronales Netz, bzw. ein Perzeptron als Spezialfall die
Parameterbestimmung für folgende DGLen 

\begin{align}
  \dot{y}&=\alpha_1 &,\  y(0)=0 \label{DGL1} &,\  & \alpha_1 \in [0,10]&,\ & t\in[0,10] \\
  \dot{y}&=\alpha_1 y &,\  y(0)=1\label{DGL2}&,\  & \alpha_1 \in [0,5]&,\ & t\in[0,1]\\
  \dot{y}&=\alpha_1 + \alpha_2 &,\  y(0)=0 \label{DGL3}&,\  & \alpha_1,\alpha_2 \in [0,10]&,\ & t\in[0,10]\\
  \dot{y}&=(\alpha_1 + \alpha_2) y &,\  y(0)=1\label{DGL4}&,\
  & \alpha_1,\alpha_2 \in [0,5]&,\ & t\in[0,1]
\end{align}

\begin{figure}[h]
\begin{center}
  \begin{tikzpicture}[node distance=1.5cm, auto]
    \node[draw, circle, minimum size=.95cm] (1) at (0,0) {};
  \node[draw, circle, minimum size=.95cm,below of=1] (2) {};
  \node[draw, circle, minimum size=.95cm, below of=2] (5) {$\textbf{b}$};
  \node[draw, circle, minimum size=.7cm, right of=2, node distance=2cm] (3) {$\sum$};
  \node[draw, circle, minimum size=.95cm, right of=3, node distance=2cm] (4)
    {$\scriptscriptstyle ReLU$};
  \node[draw, circle, minimum size=.95cm, right of=4, node distance=2cm] (6) {};
  \path
  (1) edge node[scale=0.8]{$W_1$}(3)
  (2) edge node[scale=0.8]{$W_2$}(3)
  (5) edge (3)
  (4) edge (6)
  (3) edge (4);
\end{tikzpicture}
\end{center}
  \caption{Perzeptron}
\end{figure}

vornimmt. Von besonderem Interesse ist bei dieser Untersuchung des
Lösungsverhaltens bei auftretender Redundanz in den DGLen.
Grundsätzlich gilt für alle zu untersuchenden DGLen, dass neuronale
Netze/Perzeptrons mit Aktivierungsfunktion 

\[
  ReLU(z)\coloneqq \max(0,z)
\]

und möglichst geringer Komplexität gewählt werden. 

Um das Verhalten zu beurteilen werden die jeweiligen Gewichtungsmatrizen
betrachtet und versucht, in den Gewichten ein Lösungsverfahren zu
rekonstruieren.

\section{Untersuchung der DGLen}
\textbf{Untersuchung von (\ref{DGL1}})


Die analytische Lösung von (\ref{DGL1})

\[
  y(t)=\alpha_1 t
\]

kann z.B. durch Trennung der Veränderlichen bestimmt werden.

Für (\ref{DGL1}) können folgende Gewichtungsmatrix und Bias-Vektor

\[
  \begin{aligned}
  \textbf{W}=\left(
  \begin{matrix} -0.13981831  \\
    0.09966602  
  \end{matrix}\right)
    & \quad \textbf{b}=\left(
  \begin{matrix}
    -0.00022739 
  \end{matrix}\right)
  \end{aligned}
\]

gefunden werden. Die zu diesen Werten gehörenden Mittelwerte der realen Parameter und der durch das
Perzeptron vorhergesagten sind:

\begin{table}[H]
  \centering
\begin{tabular}{l r}
Reale Parameter:& $5.019775705700607$ \\
Vorhersage:& $4.962721$ \\
\end{tabular}
\end{table}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5,trim={7cm 3cm 3cm 6cm},clip]{./Images/DGL1_Opti_landscape}
  \caption{Darstellung der Fehlerfunktion $\mathcal{J}$}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4,trim={3cm 0cm 3cm 0cm},clip]{./Images/DGL1_Opti_landscape_contour}
  \caption{Höhenlinien von $\mathcal{J}$. Erkennbar ist das Minimum bei $b=0$,
  $w=0.1$ .}
\end{figure}

Für das Training wurden dabei die Punkte $y_{0}=y(0)=0$ und $y_{1}= y(10)$
genutzt. Aus den gewählten Punkten wird deutlich, dass lediglich das zum Punkt
$y_1$ gehörende Gewicht $W_2$ relevant ist. 
Diese Gewichtsmatrix entspricht der Parameterbestimmung mittels des
Differenzenquotienten 

\[
  \dot{y}\approx \frac{y_{t+1}-y_{t}}{h}=\alpha
\]
mit $h=10$ folgt $\frac{1}{h}=\frac{1}{10}=0.1\approx 0.0996602$.



\vspace{12mm}

\end{comment}

\begin{comment}
\textbf{Untersuchung von (\ref{DGL2}})

Anders als die Lösung von (\ref{DGL1}) ist die Lösung von (\ref{DGL2})

\[
  y(t)=e^{\alpha_1 t}
\]

nicht-linear. Mit den Punkten $y_{0}=0$, $y_{1}=\frac{1}{2}$ und $y_{2}=1$
konnte mit den Gewichten und Bias

\[
  \begin{aligned}
  \textbf{W}=\left(\begin{matrix}
    -0.6227822303771972656 \\
    0.9651677012443542480 \\
    -0.04515352472662925720 \\
  \end{matrix}\right) & \quad \textbf{b}=\left(
    \begin{matrix}
      0.1297805905342102051 
    \end{matrix}\right)
  \end{aligned}
\]

der Parameter von (\ref{DGL2}) gut angenähert werden. 

\begin{table}[H]
  \centering
\begin{tabular}{l r}
Reale Parameter:& $2.497643976885193$ \\
Vorhersage:& $2.4626303$ \\
\end{tabular}
\end{table}

\begin{figure}[H]
  \centering
  \includegraphics{DGL2_KNN_fun}
  \caption{Vergleich der analtischen Umkehrfunktion mit der linearen
  Transformation des neuronalen Netzes}
\end{figure}

\vspace{12mm}

\textbf{Untersuchung von (\ref{DGL3})}

Ersichtlich ist das Problem (\ref{DGL3}) nicht eindeutig lösbar, da die
Parameter $\alpha$ und $\beta$ nicht näher bestimmt sind und aus der Lösung 

\[
  y(t)=(\alpha+\beta)y
\]

kein Rückschluss auf einen oder bei Parameter möglich ist. Jedoch ließe sich
eine aus den verfügbaren Daten eine Aussage über die Summe der Parameter
$\gamma=\alpha+\beta$ machen. Ein erwartbares Verhalten eines neuronalen Netzes
ohne verdeckte Schicht mit zwei Ausgangsneuronen wäre also ähnlich wie für das
neuronale Netz für (\ref{DGL1}). Eine Möglichkeit, die Summe der Parameter
anzunähern wäre die Gewichte, die den Wert eines der beiden Neuronen bestimmen
den Wert $0$ zuzuweisen. Eine mögliche Konfiguration für Gewichtungsmatrix und
Bias-Vektor wäre mit

\[
  \begin{aligned}
    &\textbf{W}=\left(
    \begin{matrix} 3.630998730659484863 \cdot 10^{-1} &
    9.998245537281036377 \cdot 10^{-2}\\
      0 & 0 \\
  \end{matrix}\right) \\
     &\\
     &\textbf{b}=\left(
  \begin{matrix}
    -4.624916911125183105 \cdot 10^{-1} \\
    0 \\
  \end{matrix}\right)
  \end{aligned}
\]

gegeben. Die Zahlenwerte für $\textbf{W}$ und $\textbf{b}$ sind dabei dem
Beispiel (\ref{DGL1}) entnommen und so ergänzt, dass das zusätzliche
Ausgangsneuron konstant den Wert $0$ liefert. 
Unter Berücksichtigung der Gegebenheiten wird jedoch schnell deutlich, dass eine
solche Möglichkeit im Bezug auf die verwendete Verlustfunktion nicht eintreten
wird. Stattdessen wird das neuronale Netz mit folgenden Parametern genutzt. 


$$\label{Eq:DGL3_W}
  \begin{aligned}
  \textbf{W}=\left(
  \begin{matrix}
    1.0776585 & 0.0510327 \\
    -0.685702 & 0.04899092 \\
  \end{matrix}
    \right) & \quad
    \textbf{b}=\left(
    \begin{matrix}
    -0.0021299 \\
    0.00072778 \\
  \end{matrix}
  \right)
  \end{aligned}
$$

Dies entspricht der Näherung der Summe $\alpha_1+\alpha_2$ durch zwei annähernd
gleich große Teile. Numerisch wird dies durch den Differenzenquotienten

$$\label{Eq:Diffquot_a+b}
  \frac{y_{t+1}-y_t}{h}=\alpha_1+\alpha_2 =:c
$$

mit der zusätzlichen Bedingung 
$$\label{Eq:a+b}
\alpha_1=\alpha_2=\frac{c}{2}
$$
erreicht. Nach Einsetzen von (\ref{a+b}) in (\ref{Eq:Diffquot_a+b}) und der
Anfangsbedingung $y_0=0$ folgt

\[
\alpha_1=\frac{y_{t+1}}{2h}.
\]

Dies entspricht in guter Näherung den Gewichten in (\ref{Eq:DGL3_W}).

\begin{table}[H]
  \centering
\begin{tabular}{l r r}
  Reale Parameter:& $2.46889687$ & $2.49231154$ \\
  Vorhersage:& $2.529707$ & $2.4312675$ \\
\end{tabular}
\end{table}

\end{comment}

